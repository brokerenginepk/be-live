(function(brokerengine, $, undefined){
	function ODataInterface(entity, key)
	{
		this._entity = entity;
		this._key = key;
	}
	Object.defineProperty(ODataInterface.prototype, "ServiceUrl",{
		get: function(){
			return brokerengine.odataurl;
		},
		configurable: false,
		enumerable: true
	});
	ODataInterface.prototype.updateLookup = function(item, field, lookupentity, lookupid, callercontext)
	{
		var entity = this._entity;
		var itemid = item[this._key];

		var url = this.ServiceUrl + "/" + entity + "(" + itemid + ")";

		if($.isEmptyObject(lookupid)){
			url += "/" + field + "/$ref";
			// Send delete lookup request to server
			return $.ajax({
				url: url,
				type: "DELETE",
				accepts: "application/json",
				contentType: "application/json",
				context: callercontext
			});
		}
		else{
			var newitem = {};
			newitem[field+"@odata.bind"] = this.ServiceUrl + "/" + lookupentity + "(" + lookupid + ")";
	
			// Send update data request to server
			return $.ajax({
				url: url,
				type: "PATCH",
				accepts: "application/json",
				contentType: "application/json",
				processData: false,
				dataType: "json",
				data: JSON.stringify(newitem),
				context: callercontext
			});
		}

	}
	ODataInterface.prototype.createRecord = function(newitem, callercontext){
		var entity = this._entity;

		var url = this.ServiceUrl + "/" + this._entity;
		// Send update data request to server
		return $.ajax({
			url: url,
			type: "POST",
			accepts: "application/json",
			contentType: "application/json",
			processData: false,
			dataType: "json",
			data: JSON.stringify(newitem),
			context: callercontext
		});
	}
	brokerengine.ODataInterface = ODataInterface;
})(window.brokerengine = window.brokerengine || {}, jQuery);
