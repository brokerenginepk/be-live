(function(brokerengine, $, undefined){
  (function(staticviews){
    (function(contacts){

      contacts.views = [
        { Label: "BE Views", header: true},
        { Label: "Contacts/Splits", Value:"Static:Contacts/Splits" },
        { Label: "Contacts/Deals", Value:"Static:Contacts/Deals" },
        { Label: "All Loan Splits", Value:"Static:All Loan Splits" },
        { Label: "Marketing Lists", header: true}
      ];

      // private members for use with contacts related views
      var columns = [
        { binding: "lastname", name: "Last Name", header: "Last Name", width: "5*", minWidth: 125, isReadOnly: true  },
        { binding: "firstname", name: "First Name", header: "First Name", width: "5*", minWidth: 125, isReadOnly: true  },
        { binding: "nickname", name: "Preferred Name", header: "Preferred Name", width: "5*", minWidth: 125, isReadOnly: true  },
        { binding: "mps_contacttype", name: "Contact Type", header: "Contact Type", width: "5*", minWidth: 125, isReadOnly: true  },
        { binding: "mobilephone", name: "Mobile Phone", header: "Mobile Phone", width: "5*", minWidth: 125, isReadOnly: true  },
        { binding: "emailaddress1", name: "Email", header: "Email", width: "5*", minWidth: 125, isReadOnly: true  },
        { binding: "address1_composite", name: "Residential Address", header: "Residential Address", width: "5*", minWidth: 125, isReadOnly: true  },
        { binding: "address2_composite", name: "Postal Address", header: "Postal Address", width: "5*", minWidth: 125, isReadOnly: true  },
        { binding: "birthdate", name: "DoB", header: "DoB", width: "5*", minWidth: 125, dataType: wijmo.DataType.Date, format: "dd-MM-yyyy", isReadOnly: true },
        { binding: "mps_reviewfrequency", name: "Review Frequency", header: "Review Frequency", width: "5*", minWidth: 125, isReadOnly: true  },
        { binding: "mps_reviewdate", name: "Next Review Date", header: "Next Review Date", width: "5*", minWidth: 125, dataType: wijmo.DataType.Date, format: "dd-MM-yyyy", isReadOnly: false, isRequired: false  },
        { binding: "mps_lastreviewsummary", name: "Last Review Date", header: "Last Review Date", width: "5*", minWidth: 125, dataType: wijmo.DataType.Date, format: "dd-MM-yyyy", isReadOnly: false, isRequired: false  },
        { binding: "_mps_brand_value", name: "Brand", header: "Brand", width: "5*", minWidth: 125, isReadOnly: true  },
        { binding: "_mps_referredby_value", name: "Referred By", header: "Referred By", width: "5*", minWidth: 125, isReadOnly: true  },
        { binding: "_ownerid_value", name: "Owner", header: "Broker", width: "5*", minWidth: 125, isReadOnly: true  }
      ];

      var idfield = "contactid";

      var fields = [
        "lastname",
        "firstname",
        "nickname",
        "mobilephone",
        "emailaddress1",
        "address1_composite",
        "address2_composite",
        "birthdate",
        "mps_reviewfrequency",
        "mps_reviewdate",
        "mps_lastreviewsummary",
        "_mps_brand_value",
        "mps_contacttype",
        "_mps_referredby_value",
        "_ownerid_value",
        "statecode"
      ];

      var field_datatypes = {
        birthdate: wijmo.DataType.Date,
        mps_reviewdate: wijmo.DataType.Date,
        mps_lastreviewsummary: wijmo.DataType.Date
      };

      var activefilter = "$filter=statecode eq 0";

      (function(contact_splits){
        var lender_op = {};
        var lender_val = {};
        var loanamount_op = {};
        var loanamount_val = {};
        var producttype_op = {};
        var producttype_val = {};
        var loanpurpose_op = {};
        var loanpurpose_val = {};
        var interestrate_op = {};
        var interestrate_val = {};
        var pricingdiscount_op = {};
        var pricingdiscount_val = {};
        var netrate_op = {};
        var netrate_val = {};
        var paymentoptions_op = {};
        var paymentoptions_val = {};
        var fixedrateexpritydate_op = {};
        var fixedrateexpritydate_val = {};
        var interestonlyexpirydate_op = {};
        var interestonlyexpirydate_val = {};
        var loanbsb_op = {};
        var loanbsb_val = {};
        var loanaccnum_op = {};
        var loanaccnum_val = {};

        contact_splits.initialize = function(id){
          // initialize view filter controls
          configureSubgridFilters();

          initializeGrid(id);


          var detailProvider = new wijmo.grid.detail.FlexGridDetailProvider(staticviews.grid,{
            isAnimated: true,
            createDetailCell: function(row)
            {
              return configureDetailsGrid(row.dataItem);
            }
          });
        };

        /*ns*/
        function get_today(){
          var today = new Date();
          var dd = today.getDate();
          var mm = today.getMonth()+1; //January is 0!
          var yyyy = today.getFullYear();
          if(dd<10) {dd = '0'+dd}
          if(mm<10) {mm = '0'+mm}
          today = dd + '-' + mm + '-' + yyyy;
          return today;
        }
        function clear_filter_contact_splits(){
          staticviews.showFilteredResults = false;
          staticviews.filterids = [];
          staticviews.selectedItems = [];

          lender_op.selectedIndex=0;
          loanamount_op.selectedIndex=0;
          producttype_op.selectedIndex=0;
          loanpurpose_op.selectedIndex=0;
          interestrate_op.selectedIndex=0;
          pricingdiscount_op.selectedIndex=0;
          netrate_op.selectedIndex=0;
          paymentoptions_op.selectedIndex=0;
          fixedrateexpritydate_op.selectedIndex=0;
          interestonlyexpirydate_op.selectedIndex=0;
          loanbsb_op.selectedIndex=0;
          loanaccnum_op.selectedIndex=0;

          $("#be_appview_val_lender input").val("--Select--");
          $("#be_appview_val_loanamount input").val("0");
          $("#be_appview_val_producttype input").val("--Select--");
          $("#be_appview_val_loanpurpose input").val("--Select--");
          $("#be_appview_val_interestrate input").val("0");
          $("#be_appview_val_pricingdiscount input").val("0");
          $("#be_appview_val_netrate input").val("0");
          $("#be_appview_val_paymentoptions input").val("");
          $("#be_appview_val_fixedrateexpirydate input").val(get_today());
          $("#be_appview_val_interestonlyexpirydate input").val(get_today());
          $("#be_appview_val_loanbsb input").val("");
          $("#be_appview_val_loanaccnum input").val("");
        }
        $("#be_staticviews_btn_clear").click(clear_filter_contact_splits);

        function configureDetailsGrid(itemRow){

          staticviews.startRowLoading();
          var cell = document.createElement('div');

          // This filter is applied on application entries
          var filter = "_mps_contact_value eq "+itemRow.contactid;
          var grid = brokerengine.entities.fundingpositions.generateSplitsGrid(cell, filter, false);
          grid.columns.remove("Contact");
          grid.columns.remove("PostCapLVR");
          grid.columns.remove("Owner");

          staticviews.endRowLoading();
          return cell;
        }

        var subgridFiltersUninitilized1=true;
        function configureSubgridFilters(){
          staticviews.handleSubgridButtons(applySubgridFilters);
          $("#be_staticviews_splitsfilter").show();
          $("#be_staticviews_appfilters").hide();

          if(subgridFiltersUninitilized1){
            subgridFiltersUninitilized1=false;

            // Lender
            lender_op = brokerengine.createFilterOperator("#be_appview_op_lender", wijmo.DataType.String);
            // Fetch lenders
            var lender_arr = brokerengine.entities.lenders.fetchLenders();
            lender_arr.unshift({mps_lendersid: "", lender: {imgpath:"", lenderid:"", name:"--Select--"}});
            lender_val = brokerengine.createDatamapInput("#be_appview_val_lender", lender_arr, "mps_lendersid", "lender.name");
            // Loan Amount
            loanamount_op = brokerengine.createFilterOperator("#be_appview_op_loanamount", wijmo.DataType.Number);
            loanamount_val = brokerengine.createFilterInput("#be_appview_val_loanamount", wijmo.DataType.Number, "c0");
            // Product Type
            producttype_op = brokerengine.createFilterOperator("#be_appview_op_producttype", wijmo.DataType.String);
            producttype_val = brokerengine.createDatamapInput("#be_appview_val_producttype", brokerengine.producttype, "Value", "Label");
            // LoanPurpose
            loanpurpose_op = brokerengine.createFilterOperator("#be_appview_op_loanpurpose", wijmo.DataType.String);
            loanpurpose_val = brokerengine.createDatamapInput("#be_appview_val_loanpurpose", brokerengine.purpose, "Value", "Label");
            // InterestRate
            interestrate_op = brokerengine.createFilterOperator("#be_appview_op_interestrate", wijmo.DataType.Number);
            interestrate_val = brokerengine.createFilterInput("#be_appview_val_interestrate", wijmo.DataType.Number);
            // PricingDiscount
            pricingdiscount_op = brokerengine.createFilterOperator("#be_appview_op_pricingdiscount", wijmo.DataType.Number);
            pricingdiscount_val = brokerengine.createFilterInput("#be_appview_val_pricingdiscount", wijmo.DataType.Number);
            // NetRate
            netrate_op = brokerengine.createFilterOperator("#be_appview_op_netrate", wijmo.DataType.Number);
            netrate_val = brokerengine.createFilterInput("#be_appview_val_netrate", wijmo.DataType.Number);
            // PMTOptions
            paymentoptions_op = brokerengine.createFilterOperator("#be_appview_op_paymentoptions", wijmo.DataType.String);
            paymentoptions_val = brokerengine.createFilterInput("#be_appview_val_paymentoptions", brokerengine.pmtoptions, "Value", "Label");
            // FixedRateExpiryDate
            fixedrateexpritydate_op = brokerengine.createFilterOperator("#be_appview_op_fixedrateexpritydate", wijmo.DataType.Date);
            fixedrateexpritydate_val = brokerengine.createFilterInput("#be_appview_val_fixedrateexpirydate", wijmo.DataType.Date, "dd-MM-yyyy");
            // InterestOnlyExpiryDate
            interestonlyexpirydate_op = brokerengine.createFilterOperator("#be_appview_op_interestonlyexpirydate", wijmo.DataType.Date);
            interestonlyexpirydate_val = brokerengine.createFilterInput("#be_appview_val_interestonlyexpirydate", wijmo.DataType.Date, "dd-MM-yyyy");
            // LoanBSB
            loanbsb_op = brokerengine.createFilterOperator("#be_appview_op_loanbsb", wijmo.DataType.String);
            loanbsb_val = brokerengine.createFilterInput("#be_appview_val_loanbsb", wijmo.DataType.String);
            // LoanAccNumber
            loanaccnum_op = brokerengine.createFilterOperator("#be_appview_op_loanaccnum", wijmo.DataType.String);
            loanaccnum_val = brokerengine.createFilterInput("#be_appview_val_loanaccnum", wijmo.DataType.String);
          }

        }

        function applySubgridFilters(){
          var filter = getFilterString();
          if(filter)
          {
            staticviews.showFilteredResults = true;
            staticviews.filterids = fetchFilteredContacts(filter);
            staticviews.selectedItems = [];
            staticviews.grid.itemsSource.refresh();
          }
        }

        function getFilterString(){
          var filter = ""
          var op;
          var value;

          if(lender_op.selectedIndex > 0) {
            op = lender_op.selectedValue;
            field = "_mps_lender_value";
            value = lender_val.selectedValue;
            filter += "(";
            filter += brokerengine.getFilterString(op, field, value);
            filter += ")";
          }

          if(loanamount_op.selectedIndex > 0) {
            if(filter){
              filter += " and ";
            }
            op = loanamount_op.selectedValue;
            field = "mps_loanamount";
            value = loanamount_val.value;
            filter += "(";
            filter += brokerengine.getFilterString(op, field+"1", value);
            filter += " or " + brokerengine.getFilterString(op, field+"2", value);
            filter += " or " + brokerengine.getFilterString(op, field+"3", value);
            filter += " or " + brokerengine.getFilterString(op, field+"4", value);
            filter += " or " + brokerengine.getFilterString(op, field+"5", value);
            filter += ")";
          }

          if(producttype_op.selectedIndex > 0) {
            if(filter) {
              filter += " and ";
            }
            op = producttype_op.selectedValue;
            field = "mps_producttypeloantype";
            value = producttype_val.selectedValue;
            filter += "(";
            filter += brokerengine.getFilterString(op, field+"1", value);
            filter += " or " + brokerengine.getFilterString(op, field+"2", value);
            filter += " or " + brokerengine.getFilterString(op, field+"3", value);
            filter += " or " + brokerengine.getFilterString(op, field+"4", value);
            filter += " or " + brokerengine.getFilterString(op, field+"5", value);
            filter += ")";
          }

          if(loanpurpose_op.selectedIndex > 0){
            if(filter){
              filter += " and ";
            }
            op = loanpurpose_op.selectedValue;
            field = "mps_loanpurposesplit";
            value = loanpurpose_val.selectedValue;
            filter += "(";
            filter += brokerengine.getFilterString(op, field+"1", value);
            filter += " or " + brokerengine.getFilterString(op, field+"2", value);
            filter += " or " + brokerengine.getFilterString(op, field+"3", value);
            filter += " or " + brokerengine.getFilterString(op, field+"4", value);
            filter += " or " + brokerengine.getFilterString(op, field+"5", value);
            filter += ")";
          }

          if(interestrate_op.selectedIndex > 0){
            if(filter){
              filter += " and ";
            }
            op = interestrate_op.selectedValue;
            field = "mps_interestrate";
            value = interestrate_val.value;
            filter += "(";
            filter += brokerengine.getFilterString(op, field+"1", value);
            filter += " or " + brokerengine.getFilterString(op, field+"2", value);
            filter += " or " + brokerengine.getFilterString(op, field+"3", value);
            filter += " or " + brokerengine.getFilterString(op, field+"4", value);
            filter += " or " + brokerengine.getFilterString(op, field+"5", value);
            filter += ")";
          }

          if(pricingdiscount_op.selectedIndex > 0){
            if(filter){
              filter += " and ";
            }
            op = pricingdiscount_op.selectedValue;
            field = "mps_pricingdiscount";
            value = pricingdiscount_val.value;
            filter += "(";
            filter += brokerengine.getFilterString(op, field+"1", value);
            filter += " or " + brokerengine.getFilterString(op, field+"2", value);
            filter += " or " + brokerengine.getFilterString(op, field+"3", value);
            filter += " or " + brokerengine.getFilterString(op, field+"4", value);
            filter += " or " + brokerengine.getFilterString(op, field+"5", value);
            filter += ")";
          }

          if(netrate_op.selectedIndex > 0){
            if(filter){
              filter += " and ";
            }
            op = netrate_op.selectedValue;
            field = "mps_netrate";
            value = netrate_val.value;
            filter += "(";
            filter += brokerengine.getFilterString(op, field+"1", value);
            filter += " or " + brokerengine.getFilterString(op, field+"2", value);
            filter += " or " + brokerengine.getFilterString(op, field+"3", value);
            filter += " or " + brokerengine.getFilterString(op, field+"4", value);
            filter += " or " + brokerengine.getFilterString(op, field+"5", value);
            filter += ")";
          }

          if(paymentoptions_op.selectedIndex > 0){
            if(filter){
              filter += " and ";
            }
            op = paymentoptions_op.selectedValue;
            field = "mps_pmtoption";
            value = paymentoptions_val.value;
            filter += "(";
            filter += brokerengine.getFilterString(op, field+"1", value);
            filter += " or " + brokerengine.getFilterString(op, field+"2", value);
            filter += " or " + brokerengine.getFilterString(op, field+"3", value);
            filter += " or " + brokerengine.getFilterString(op, field+"4", value);
            filter += " or " + brokerengine.getFilterString(op, field+"5", value);
            filter += ")";
          }

          // TO BE FIXED FOR DATE FILTERS
          if(fixedrateexpritydate_op.selectedIndex > 0){
            if(filter) {
              filter += " and ";
            }
            op = fixedrateexpritydate_op.selectedValue;
            field = "mps_fixedrateexpirydate";
            value = fixedrateexpritydate_val.value;
            filter += "(";
            filter += brokerengine.getFilterDate(op, field+"1", value);
            filter += " or " + brokerengine.getFilterDate(op, field+"2", value);
            filter += " or " + brokerengine.getFilterDate(op, field+"3", value);
            filter += " or " + brokerengine.getFilterDate(op, field+"4", value);
            filter += " or " + brokerengine.getFilterDate(op, field+"5", value);
            filter += ")";
          }

          if(interestonlyexpirydate_op.selectedIndex > 0){
            if(filter){
              filter += " and ";
            }
            op = interestonlyexpirydate_op.selectedValue;
            field = "mps_interestonlyexpirydate";
            value = interestonlyexpirydate_val.value;
            filter += "(";
            filter += brokerengine.getFilterDate(op, field+"1", value);
            filter += " or " + brokerengine.getFilterDate(op, field+"2", value);
            filter += " or " + brokerengine.getFilterDate(op, field+"3", value);
            filter += " or " + brokerengine.getFilterDate(op, field+"4", value);
            filter += " or " + brokerengine.getFilterDate(op, field+"5", value);
            filter += ")";
          }

          if(loanbsb_op.selectedIndex > 0) {
            if(filter) {
              filter += " and ";
            }
            op = loanbsb_op.selectedValue;
            field = "mps_loanbsb";
            value = loanbsb_val.value;
            filter += "(";
            filter += brokerengine.getFilterString(op, field+"1", value);
            filter += " or " + brokerengine.getFilterString(op, field+"2", value);
            filter += " or " + brokerengine.getFilterString(op, field+"3", value);
            filter += " or " + brokerengine.getFilterString(op, field+"4", value);
            filter += " or " + brokerengine.getFilterString(op, field+"5", value);
            filter += ")";
          }

          if(loanaccnum_op.selectedIndex > 0){
            if(filter){
              filter += " and ";
            }
            op = loanaccnum_op.selectedValue;
            field = "mps_loanaccountnumber";
            value = loanaccnum_val.value;
            filter += "(";
            filter += brokerengine.getFilterString(op, field+"1", value);
            filter += " or " + brokerengine.getFilterString(op, field+"2", value);
            filter += " or " + brokerengine.getFilterString(op, field+"3", value);
            filter += " or " + brokerengine.getFilterString(op, field+"4", value);
            filter += " or " + brokerengine.getFilterString(op, field+"5", value);
            filter += ")";
          }

          if(filter){
            filter += " and (mps_ApplicationId ne null)";
            filter += " and (mps_Contact ne null)";
            filter += " and (mps_defaultfundingposition eq 922600000)";
          }

          return filter;
        }

        function fetchFilteredContacts(filter){
          var url = brokerengine.odataurl;
          var contacts_array = [];

          url +=  "/mps_fundingpositions?$select=_mps_contact_value&$filter="+filter;
          //console.log(url);
          $.ajax({
            async: false,
            dataType: "json",
            url: url,
            success: function(data){
              data.value.forEach(function(fp){
                contacts_array.push(fp._mps_contact_value)
              });
            }
          });

          //console.log(contacts_array);
          return contacts_array;
        }

      })(contacts.splitssub = contacts.splitssub || {} );

      (function(contact_apps){
        var lender_op = {};
        var lender_val = {};
        var lenderref_op = {};
        var lenderref_val = {};
        var appstage_op = {};
        var appstage_val = {};
        var loanamount_op = {};
        var loanamount_val = {};
        var financedate_op = {};
        var financedate_val = {};
        var settlementdate_op = {};
        var settlementdate_val = {};
        var stageduedate_op = {};
        var stageduedate_val = {};
        var owner_op = {};
        var owner_val = {};
        var team_op = {};
        var team_val = {};
        var createdon_op = {};
        var createdon_val = {};

        contact_apps.initialize = function(id){
          // initialize view filter controls
          configureSubgridFilters();

          initializeGrid(id);

          var detailProvider = new wijmo.grid.detail.FlexGridDetailProvider(staticviews.grid,{
            isAnimated: true,
            createDetailCell: function(row)
            {
              return configureDetailsGrid(row.dataItem);
            }
          });

        };

        /*ns*/
        function get_today(){
          var today = new Date();
          var dd = today.getDate();
          var mm = today.getMonth()+1; //January is 0!
          var yyyy = today.getFullYear();
          if(dd<10) {dd = '0'+dd}
          if(mm<10) {mm = '0'+mm}
          today = dd + '-' + mm + '-' + yyyy;
          return today;
        }
        function clear_filter_contact_app(){
          staticviews.showFilteredResults = false;
          staticviews.filterids = [];
          staticviews.selectedItems = [];

          lender_op.selectedIndex=0;
          lenderref_op.selectedIndex=0;
          appstage_op.selectedIndex=0;
          loanamount_op.selectedIndex=0;
          financedate_op.selectedIndex=0;
          settlementdate_op.selectedIndex=0;
          stageduedate_op.selectedIndex=0;
          owner_op.selectedIndex=0;
          team_op.selectedIndex=0;
          createdon_op.selectedIndex=0;
          $("#be_contactview_val_lender input").val("--Select--");
          $("#be_contactview_val_lenderref input").val("");
          $("#be_contactview_val_appstage input").val("--Select--");
          $("#be_contactview_val_loanamount input").val("$0");
          $("#be_contactview_val_financedate input").val(get_today());
          $("#be_contactview_val_settlementdate input").val(get_today());
          $("#be_contactview_val_stageduedate input").val(get_today());
          $("#be_contactview_val_owner input").val("--Select--");
          $("#be_contactview_val_managingteam input").val("--Select--");
          $("#be_contactview_val_createdon input").val(get_today());
        }
        $("#be_staticviews_btn_clear").click(clear_filter_contact_app);

        function configureDetailsGrid(itemRow){
          staticviews.startRowLoading();

          var cell = document.createElement('div');

          var filter = "_mps_contact_value eq "+itemRow.contactid;
          var grid = brokerengine.entities.applications.generateGrid(cell, filter, false);
          grid.columns.remove("Contact");

          staticviews.endRowLoading();
          return cell;
        }

        var subgridFiltersUninitilized2=true;
        function configureSubgridFilters(){
          staticviews.handleSubgridButtons(applySubgridFilters);
          $("#be_staticviews_splitsfilter").hide();
          $("#be_staticviews_appfilters").show();

          if(subgridFiltersUninitilized2){
            subgridFiltersUninitilized2=false;

            // Lender
            lender_op = brokerengine.createFilterOperator("#be_contactview_op_lender", wijmo.DataType.String);
            // Fetch lenders
            var lender_arr = brokerengine.entities.lenders.fetchLenders();
            lender_arr.unshift({mps_lendersid: "", lender: {imgpath:"", lenderid:"", name:"--Select--"}});
            lender_val = brokerengine.createDatamapInput("#be_contactview_val_lender", lender_arr, "mps_lendersid", "lender.name");
            // Lender Ref
            lenderref_op = brokerengine.createFilterOperator("#be_contactview_op_lenderref", wijmo.DataType.String);
            lenderref_val = brokerengine.createFilterInput("#be_contactview_val_lenderref", wijmo.DataType.String);
            // Application stage
            appstage_op = brokerengine.createFilterOperator("#be_contactview_op_appstage", wijmo.DataType.String);
            // Fetch stages
            var stage_arr = brokerengine.entities.applicationstages.fetchStages();
            stage_arr.unshift({mps_stageid: "", mps_stage1: "--Select--" });
            appstage_val = brokerengine.createDatamapInput("#be_contactview_val_appstage", stage_arr, "mps_stageid", "mps_stage1");
            // Loan Amount
            loanamount_op = brokerengine.createFilterOperator("#be_contactview_op_loanamount", wijmo.DataType.Number);
            loanamount_val = brokerengine.createFilterInput("#be_contactview_val_loanamount", wijmo.DataType.Number, "c0");
            // Finance Date
            financedate_op = brokerengine.createFilterOperator("#be_contactview_op_financedate", wijmo.DataType.Date);
            financedate_val = brokerengine.createFilterInput("#be_contactview_val_financedate", wijmo.DataType.Date, "dd-MM-yyyy");
            // Settlement Date
            settlementdate_op = brokerengine.createFilterOperator("#be_contactview_op_settlementdate", wijmo.DataType.Date);
            settlementdate_val = brokerengine.createFilterInput("#be_contactview_val_settlementdate", wijmo.DataType.Date, "dd-MM-yyyy");
            // Stage Due Date
            stageduedate_op = brokerengine.createFilterOperator("#be_contactview_op_stageduedate", wijmo.DataType.Date);
            stageduedate_val = brokerengine.createFilterInput("#be_contactview_val_stageduedate", wijmo.DataType.Date, "dd-MM-yyyy");
            // Owner
            owner_op = brokerengine.createFilterOperator("#be_contactview_op_owner", wijmo.DataType.String);
            // Fetch users
            var user_arr = brokerengine.entities.users.fetch();
            user_arr.unshift({fullname:"--Select--", systemuserid:""});
            owner_val = brokerengine.createDatamapInput("#be_contactview_val_owner", user_arr, "systemuserid", "fullname");
            // Managing Team
            team_op = brokerengine.createFilterOperator("#be_contactview_op_managingteam", wijmo.DataType.String);
            // Fetch teams
            var team_arr = brokerengine.entities.teams.fetch();
            team_arr.unshift({name:"--Select--", teamid:""});
            team_val = brokerengine.createDatamapInput("#be_contactview_val_managingteam", team_arr, "teamid", "name");
            // Created On
            createdon_op = brokerengine.createFilterOperator("#be_contactview_op_createdon", wijmo.DataType.Date);
            createdon_val = brokerengine.createFilterInput("#be_contactview_val_createdon", wijmo.DataType.Date, "dd-MM-yyyy");
          }
        }

        function applySubgridFilters(){
          var filter = getFilterString();
          if(filter)
          {
            staticviews.showFilteredResults = true;
            staticviews.filterids = fetchFilteredContacts(filter);
            staticviews.selectedItems = [];
            staticviews.grid.itemsSource.refresh();
          }
        }

        function getFilterString(){
          var filter = ""
          var op;
          var value;
          if(lender_op.selectedIndex > 0){
            op = lender_op.selectedValue;
            field = "_mps_lender_value";
            value = lender_val.selectedValue;
            filter += "(";
            filter += brokerengine.getFilterString(op, field, value);
            filter += ")";
          }

          if(lenderref_op.selectedIndex > 0){
            if(filter){
              filter += " and ";
            }
            op = lenderref_op.selectedValue;
            field = "mps_lenderreference";
            value = lenderref_val.value;
            filter += "(";
            filter += brokerengine.getFilterString(op, field, value);
            filter += ")";
          }

          if(appstage_op.selectedIndex > 0){
            if(filter){
              filter += " and ";
            }
            op = appstage_op.selectedValue;
            field = "_mps_applicationstage_value";
            value = appstage_val.selectedValue;
            filter += "(";
            filter += brokerengine.getFilterString(op, field, value);
            filter += ")";
          }

          if(loanamount_op.selectedIndex > 0){
            if(filter){
              filter += " and ";
            }
            op = loanamount_op.selectedValue;
            field = "mps_loanamount";
            value = loanamount_val.value;
            filter += "(";
            filter += brokerengine.getFilterString(op, field, value);
            filter += ")";
          }

          if(financedate_op.selectedIndex > 0){
            // TO BE FIXED FOR DATE FILTER
            if(filter){
              filter += " and ";
            }
            op = financedate_op.selectedValue;
            field = "mps_financedate";
            value = financedate_val.value;
            filter += "(";
            filter += brokerengine.getFilterDate(op, field, value);
            filter += ")";
          }

          if(settlementdate_op.selectedIndex > 0){
            // TO BE FIXED FOR DATE FILTER
            if(filter){
              filter += " and ";
            }
            op = settlementdate_op.selectedValue;
            field = "mps_settlementdate";
            value = settlementdate_val.value;
            filter += "(";
            filter += brokerengine.getFilterDate(op, field, value);
            filter += ")";
          }

          if(stageduedate_op.selectedIndex > 0){
            // TO BE FIXED FOR DATE FILTER
            if(filter){
              filter += " and ";
            }
            op = stageduedate_op.selectedValue;
            field = "mps_stageduedate";
            value = stageduedate_val.value;
            filter += "(";
            filter += brokerengine.getFilterDate(op, field, value);
            filter += ")";
          }

          if(owner_op.selectedIndex > 0) {
            if(filter){
              filter += " and ";
            }
            op = owner_op.selectedValue;
            field = "_ownerid_value";
            value = owner_val.selectedValue;
            filter += "(";
            filter += brokerengine.getFilterString(op, field, value);
            filter += ")";
          }

          if(team_op.selectedIndex > 0) {
            if(filter){
              filter += " and ";
            }
            op = team_op.selectedValue;
            field = "_mps_managingteam_value";
            value = team_val.selectedValue;
            filter += "(";
            filter += brokerengine.getFilterString(op, field, value);
            filter += ")";
          }

          if(createdon_op.selectedIndex > 0) {
            // TO BE FIXED FOR DATE FILTER
            if(filter){
              filter += " and ";
            }
            op = createdon_op.selectedValue;
            field = "mps_stageduedate";
            value = createdon_val.value;
            filter += "(";
            filter += brokerengine.getFilterDate(op, field, value);
            filter += ")";
          }

          if(filter){
            filter += " and (mps_Contact ne null)";
          }
          return filter;
        }

        function fetchFilteredContacts(filter){
          var url = brokerengine.odataurl;
          var contacts_array = [];

          url +=  "/mps_applications?$select=_mps_contact_value&$filter="+filter;
          //console.log(url);
          $.ajax({
            async: false,
            dataType: "json",
            url: url,
            success: function(data){
              data.value.forEach(function(item){
                contacts_array.push(item._mps_contact_value)
              });
            }
          });

          //console.log(contacts_array);
          return contacts_array;
        }
      })(contacts.apps = contacts.apps || {} );

      (function(splitsonly){
        splitsonly.initialize = function(id){

          staticviews.refresh_view();
          staticviews.clearSubgridButtons();

          // Get Deal splits only. This will filter out splits for leads that have not been converted into deals
          var filter = "_mps_application_value%20ne%20null";
          staticviews.grid = brokerengine.entities.fundingpositions.generateSplitsGrid(id, filter);// filter=null, odata=false
          staticviews.filter = new wijmo.grid.filter.FlexGridFilter(staticviews.grid);
          //staticviews.grid.itemsSource.filter = subgridFilter;

          staticviews.grid.rowHeaders.columns.splice(0, 1);
          staticviews.grid.rowHeaders.columns.push(new wijmo.grid.Column(staticviews.rowSelectionColumn));
          staticviews.grid.selectionChanged.addHandler(staticviews.handleSelectionChange);

          staticviews.grid.rows.minSize = 35;
          staticviews.grid.columnHeaders.rows.defaultSize = 42;
          staticviews.grid.frozenColumns=2;
          staticviews.grid.isReadOnly=true;

        };

      })(contacts.splitsonly = contacts.splitsonly || {} );

      function fetchMarketingLists(){
        var url = brokerengine.odataurl;
        var list = {};
        $.getJSON(url + "/lists?$select=listname")
        .done(function(data){
          $.each(data.value, function(i, item){
            list = new Object();
            list.Label = item.listname;
            list.Value = "List:" + item.listid;

            contacts.views.push(list);
          });
        });
      }

      contacts.initializeView = function(){
        var selectView = {};
        selectView = brokerengine.createDatamapInput("#be_staticviews_selectview", contacts.views, "Value", "Label");
        selectView.formatItem.addHandler(brokerengine.formatComboBox);
        selectView.selectedIndex = 1;

        fetchMarketingLists();

        selectView.textChanged.addHandler(changeView);
        contacts.selectView = selectView;

        brokerengine.staticviews.title = "Contacts/Splits";
        $("#be_staticviews_title").html("<H1> Contacts/Splits </H1>");
        brokerengine.staticviews.contacts.splitssub.initialize("#be_view");

        staticviews.handleExportButtons();
        staticviews.handleDeactivateButton(deactivateRecords);
      }

      function changeView(sv, ev)
      {
          var newview = contacts.selectView.selectedItem;
          var view_split = newview.Value.split(":");
          switch(view_split[0]){
            case "Static":
            switch(view_split[1]){
              case "Contacts/Splits":
              brokerengine.staticviews.title = "Contacts/Splits";
              $("#be_staticviews_title").html("<H1> Contacts/Splits </H1>");
              $("#be_staticviews_expandfilters").show();
              brokerengine.staticviews.contacts.splitssub.initialize("#be_view");
              $("#be_staticviews_deactivate").show();
              break;
              case "Contacts/Deals":
              brokerengine.staticviews.title = "Contacts/Deals";
              $("#be_staticviews_title").html("<H1> Contacts/Deals </H1>");
              $("#be_staticviews_expandfilters").show();
              brokerengine.staticviews.contacts.apps.initialize("#be_view");
              $("#be_staticviews_deactivate").show();
              break;
              case "All Loan Splits":
              brokerengine.staticviews.title = "All Loan Splits";
              $("#be_staticviews_title").html("<H1> All Loan Splits </H1>");
              $("#be_staticviews_expandfilters").hide();
              brokerengine.staticviews.contacts.splitsonly.initialize("#be_view");
              $("#be_staticviews_deactivate").hide();
              break;
            }
            break;
            case "List":
              var title = newview.Label;
              brokerengine.staticviews.title = title;
              $("#be_staticviews_title").html("<H1> " + title + " </H1>");
              $("#be_staticviews_expandfilters").hide();
              initializeMarketingListView("#be_view", view_split[1]);
            break;
          }
      }

      function initializeGrid(id, entitypath){
        brokerengine.staticviews.startRowLoading();

        staticviews.refresh_view();

        if(!entitypath){
          entitypath = "contacts";
        }

        // view should use odata interface
        var url = brokerengine.odataurl;
        var view = new wijmo.odata.ODataCollectionView(url, entitypath, {
          fields: fields,
          dataTypes: field_datatypes,
          keys: [idfield],
          filter: subgridFilter,
          oDataVersion: 4.0,
          sortOnServer: false,
          filterOnServer: false,
          loading: staticviews.startRowLoading,
          loaded: staticviews.endRowLoading,

        });
        view.sortDescriptions.push(new wijmo.collections.SortDescription("lastname", true));

        staticviews.grid = new wijmo.grid.FlexGrid(id,{
          columns: columns,
          autoGenerateColumns: false,
          itemsSource: view,
          itemFormatter: itemFormatter,
          selectionMode: wijmo.grid.SelectionMode.ListBox,
          //					loadingRows: staticviews.startRowLoading,
          //					loadedRows: staticviews.endRowLoading,
          selectionChanged: staticviews.handleSelectionChange,
          showAlternatingRows: true
        });
        staticviews.grid.rows.minSize = 35;
        staticviews.grid.columnHeaders.rows.defaultSize = 42;
        staticviews.grid.frozenColumns=2;
        new CustomGridEditor(staticviews.grid, 'mps_reviewdate', wijmo.input.InputDate, {
          format: 'dd-MM-yyyy'
        });
        //InterestOnlyExpiryDate
        new CustomGridEditor(staticviews.grid, 'mps_lastreviewsummary', wijmo.input.InputDate, {
          format: 'dd-MM-yyyy'
        });

        staticviews.grid.rowHeaders.columns.push(new wijmo.grid.Column(staticviews.rowSelectionColumn));
        staticviews.filter = new wijmo.grid.filter.FlexGridFilter(staticviews.grid);

        brokerengine.entities.contacts.assignColumnMap(staticviews.grid, "Review Frequency");
        brokerengine.entities.contacts.assignColumnMap(staticviews.grid, "Contact Type");
        brokerengine.entities.contacts.assignContactsMap(staticviews.grid, "Referred By");
        brokerengine.entities.users.assignColumnMap(staticviews.grid, "Owner");
        brokerengine.entities.brands.assignColumnMap(staticviews.grid, "Brand");

        brokerengine.staticviews.endRowLoading();
      }

      function subgridFilter(contact){
        var display = staticviews.filter._filter(contact) && contact.statecode==0;
        if(staticviews.showFilteredResults)
        {
          return (staticviews.filterids.indexOf(contact.contactid) > -1) && display;
        }
        return display;
      }

      function itemFormatter(panel, r, c, cell){
        if(panel.rows[r] instanceof wijmo.grid.detail.DetailRow)
        return;

        var col = panel.columns[c];
        if(panel.cellType == wijmo.grid.CellType.TopLeft || panel.cellType == wijmo.grid.CellType.RowHeader)
        {
          staticviews.headerFormatter(panel, r, c, cell);
        }
        else if (panel.cellType == wijmo.grid.CellType.Cell && !(panel.rows[r] instanceof wijmo.grid.detail.DetailRow))
        {
          switch (col.name)
          {
            case 'Last Name':
            if(panel.rows[r].dataItem[idfield])
            {
              var id = panel.rows[r].dataItem[idfield];
              var name = panel.rows[r].dataItem["lastname"];
              var contact = new brokerengine.entities.Contact(id, name);
              cell.innerHTML = contact.getItemUrl();
            }
            break;
            case "Email":
            if(panel.rows[r].dataItem["emailaddress1"])
            {
              cell.innerHTML = "<a href='mailto:" + panel.rows[r].dataItem["emailaddress1"] + "'>" + panel.rows[r].dataItem["emailaddress1"] + "</a>"
            }
            break;

            case "Next Review Date":
            var cellentry=cell.innerHTML.trim();
            if(cellentry){
              dts=cellentry.split("-");
              var dt = new Date(dts[2], dts[1] - 1, dts[0]);
              var today=new Date();
              var dif=(dt-today)/(3600*1000*24);
              cl="";
              if(dif<0)
                cl="redish";
              cell.innerHTML="<span class='date-cell "+cl+"'>"+cellentry+"</span>";
            }
            cell.innerHTML += "<span class='date-edit wj-glyph-pencil'></span>";
            break;

            case "Last Review Date":
            cell.innerHTML += "<span class='date-edit wj-glyph-pencil'></span>";            
            break;
          }
        }
      }

      function initializeMarketingListView(elemid, listid){
        staticviews.clearSubgridButtons();
        var listpath = "lists(" + listid + ")/listcontact_association";
        initializeGrid(elemid, listpath)
      }

      function deactivateRecord(ent, id) {
        var entity = {};
        entity.statuscode = 2;
        entity.statecode = 1;
        var req = new XMLHttpRequest();
        req.open("PATCH", brokerengine.odataurl+"/"+
          ent+"("+id+")", true);
        req.setRequestHeader("OData-MaxVersion", "4.0");
        req.setRequestHeader("OData-Version", "4.0");
        req.setRequestHeader("Accept", "application/json");
        req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
        req.onreadystatechange = function () {
          if (this.readyState === 4) {
            req.onreadystatechange = null;
            if (this.status === 204) {
            }
            else {
              Xrm.Utility.alertDialog(this.statusText);
            }
          }
        };
        req.send(JSON.stringify(entity));
      }

      deactivateRecords=function() {
        if (confirm("Are you sure that you want to delete the selected records?")) {
          var height=staticviews.grid.cells.rows.length;
          for (var i=0;i<height;i++){
            if(staticviews.grid.rows[i].isSelected){
              deactivateRecord("contacts",staticviews.grid.rows[i].dataItem.contactid);
            }
          }
          // Reload the view
          setTimeout(function(){ changeView();}, 3000);
        }
      }

    })(staticviews.contacts = staticviews.contacts || {});
  })(brokerengine.staticviews = brokerengine.staticviews || {});
})(window.brokerengine = window.brokerengine || {}, jQuery);
