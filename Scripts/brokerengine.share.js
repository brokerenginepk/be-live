(function(brokerengine, $, undefined){
	brokerengine.shareuri = brokerengine.baseurl + "/_grid/cmds/dlg_share.aspx";

	(function(share){
		share.relatedRecords = {};
		share.ShareDeals = function(dealids, relatedrecords){
			var selcount = dealids.length;
			if(selcount == 0)
			{
				alert("Select a deal to share!");
				return;
			}
			share.relatedrecords = relatedrecords;
			var dialogArguments = new Array();
			dealids.forEach(function(id){
			  dialogArguments.push("{" + id + "}");
			});
			var src = brokerengine.shareuri + "?dType=1&iObjType=10054&iTotal=" + selcount; //1&sIds=%7b53A1C7BE-2DD9-E711-8428-005056BFA573%7d";
			var dialogoptions = new Xrm.DialogOptions();
			dialogoptions.width = 800;
			dialogoptions.height = 400;
			Xrm.Internal.openDialog(src, dialogoptions, dialogArguments, null, null);
				/*
			Xrm.Internal.openDialog(src, dialogoptions, dialogArguments, null, function(retval){
				if(retval)
				{
					ShareContacts(share.relatedrecords.contacts);
				}
			});
			*/
		}

		function ShareContacts(contactids)
		{
			var selcount = contactids.length;
			if(selcount == 0)
			{
				return;
			}
			var dialogArguments = new Array();
			contactids.forEach(function(id){
			  dialogArguments.push("{" + id + "}");
			});
			var src = brokerengine.shareuri + "?dType=1&iObjType=2&iTotal=" + selcount; //1&sIds=%7b53A1C7BE-2DD9-E711-8428-005056BFA573%7d";
			var dialogoptions = new Xrm.DialogOptions();
			dialogoptions.width = 800;
			dialogoptions.height = 400;
			Xrm.Internal.openDialog(src, dialogoptions, dialogArguments, null, function(retval){
				if(retval)
				{
					var url = brokerengine.odataurl + "/contacts?$select=_mps_brand_value&$filter=Microsoft.Dynamics.CRM.In(PropertyName=%27contactid%27,%20PropertyValues=" + JSON.stringify(contactids) + ")";
					brokerengine.getData(url, function(contacts){
						var brands = [];
						var brandid;
						contacts.forEach(function(contact){
							brandid = contact._mps_brand_value;
							if(brandid && !brands.includes(brandid))
							{
								brands.push(brandid);
							}

						});
						ShareBrands(brands);
					});
				}
			});
		}

		function ShareBrands(brandids)
		{
			var selcount = brandids.length;
			if(selcount == 0)
			{
				return;
			}
			var dialogArguments = new Array();
			brandids.forEach(function(id){
			  dialogArguments.push("{" + id + "}");
			});
			var src = brokerengine.shareuri + "?dType=1&iObjType=10122&iTotal=" + selcount; //1&sIds=%7b53A1C7BE-2DD9-E711-8428-005056BFA573%7d";
			var dialogoptions = new Xrm.DialogOptions();
			dialogoptions.width = 800;
			dialogoptions.height = 400;
			Xrm.Internal.openDialog(src, dialogoptions, dialogArguments, null, function(retval){
				if(retval)
				{
					ShareBrokers(share.relatedrecords.lenderdetails);
				}
			});
		}

		function ShareBrokers(lenderdetails)
		{
			var selcount = lenderdetails.length;
			if(selcount == 0)
			{
				return;
			}
			var dialogArguments = new Array();
			lenderdetails.forEach(function(id){
			  dialogArguments.push("{" + id + "}");
			});
			var src = brokerengine.shareuri + "?dType=1&iObjType=10093&iTotal=" + selcount; //1&sIds=%7b53A1C7BE-2DD9-E711-8428-005056BFA573%7d";
			var dialogoptions = new Xrm.DialogOptions();
			dialogoptions.width = 800;
			dialogoptions.height = 400;
			Xrm.Internal.openDialog(src, dialogoptions, dialogArguments, null, null);
		}

		function ShareUI(opt){

			this.elemSelect = opt.elemSelect;
			this.elemShared = opt.elemShared;
			this.btnAdd = opt.btnAdd;
			this.btnRemove = opt.btnRemove;
			this.btnDone = opt.btnDone;
			this.onAddShare = opt.onAddShare;
			this.onRemoveShare = opt.onRemoveShare;
	
			this.teamSelect = {};
			this.shareList = {};
			this.shareTeams = [];
			this.teamNames = [];
			this.shareDialog = {};
			var _this = this;
	
			ShareUI.prototype.initializeDialog = function(){
				this.shareDialog = $("#be-share-dialog").dialog({
					autoOpen: false,
					modal: true,
					width: 600,
					})
					.dialog("instance");
				$(this.btnDone).click(function(){
						_this.shareDialog.close();
						_this.emptyShare();
					});
				this.initializeForm();
			}

			ShareUI.prototype.initializeForm = function(){
				//var accessteams = [];

				/*
					TO BE IMPLEMENTED LATER
				var shares = this.fetchShares();
				var teams = accessteams.filter(function(a){
					return shares.findIndex(function(s){
						if(s.id == a.id)
						return true;
					}) == -1;
				});

				*/
				brokerengine.entities.teams.fetchAccessTeams(this.fetchAccessTeams);
	
				this.shareList = new wijmo.input.ListBox(this.elemShared, {
					itemsSource: this.shareTeams,
					displayMemberPath: "name",
					selectedValuePath: "teamid",
					checkedMemberPath: "selected"
				});
				this.shareList.formatItem.addHandler(brokerengine.formatListBox);

				$(this.btnAdd).click(this.onAddShare);
				$(this.btnRemove).click(this.onRemoveShare);
			}

			ShareUI.prototype.fetchAccessTeams = function(teams){
					teams.forEach(function(team){
						_this.teamNames[team.teamid] = team.name;
					});
					_this.teamSelect = new wijmo.input.ComboBox(_this.elemSelect,{
					itemsSource: teams,
					placeholder: "select a team to share with",
					displayMemberPath: "name",
					selectedValuePath: "teamid",
					isEditable: false
				});
				// Bala's team need the drop down assigned to following object
				AccessTeamPublic=_this.teamSelect ;

			}

			ShareUI.prototype.addShare = function(share){
				if(!share.name){
					share.name = _this.teamNames[share.teamid];
				}

				_this.shareTeams.push(share);
				_this.shareList.refresh();
			}

			ShareUI.prototype.emptyShare = function(){
				this.shareTeams.splice(0, this.shareTeams.length);
				this.shareList.refresh();
			}

			ShareUI.prototype.getSelectedShares = function(){
				var selectedShares = [];
				this.shareTeams.forEach(function(share){
					if(share.selected){
						selectedShares.push(share);
					}
				});
				return selectedShares;
			}

			ShareUI.prototype.openDialog = function(){
				_this.shareDialog.open();
			}

			ShareUI.prototype.enableShare = function(enable){
				$(this.btnAdd).prop("disabled", !enable)
			}
		}
		brokerengine.share.ShareUI = ShareUI;

	})(brokerengine.share = brokerengine.share || {});

})(window.brokerengine = window.brokerengine || {}, jQuery);
