(function(brokerengine, $, undefined){
  (function(staticviews)  {
    (function(queues){
      var queue_columns = [
        { binding: "title", name: "Task", header: "Task", width: "8*", minWidth: 250 },
        { binding: "a_bffc8b0dc2e6e51180f5408d5c2a8c56_x002e_fullname", name: "Client", header: "Client", width: "8*", minWidth: 250 },
        { binding: "aa_x002e_ownerid", name: "Broker", header: "Broker", width: "5*", minWidth: 125 },
        { binding: "aa_x002e_mps_managingteam", name: "Managing Team", header: "Managing Team", width: "5*", minWidth: 125 },
        { binding: "aa_x002e_mps_tasktypelookup", name: "Task Type", header: "Task Type", width: "5*", minWidth: 125 },
        { binding: "aa_x002e_scheduledend", name: "Task Due Date", header: "Task Due Date", width: "5*", minWidth: 125, dataType: wijmo.DataType.Date, format: "dd-MM-yyyy" },
        { binding: "_mps_lender_value", name: "Lender", header: "Lender", width: "5*", minWidth: 80 },
        { binding: "_queueid_value", name: "Queue", header: "Queue", width: "5*", minWidth: 125 },
        { binding: "_workerid_value", name: "Worked By", header: "Worked By", width: "5*", minWidth: 125 },
      ];

      queues.initialize = function(id){
        brokerengine.staticviews.startRowLoading();

        staticviews.refresh_view();

        // view should use odata interface. For now go with static view from ajax request
        var url = brokerengine.odataurl;
        url += "/queueitems?savedQuery=086f2897-8ef2-e511-80be-408d5c9288ab";
        var queue_arr;
        $.ajax({
          async: false,
          dataType: "json",
          url: url,
          success: function(data)
          {
            queue_arr = data.value;
            queue_arr.forEach(function(task)
            {
              if(task.aa_x002e_scheduledend)
              {
                task.aa_x002e_scheduledend = new Date(task.aa_x002e_scheduledend);
              }
              task.colselect = false;
            });
          }
        });
        var qview = new wijmo.collections.CollectionView(queue_arr);
        var sort = new wijmo.collections.SortDescription("aa_x002e_scheduledend", false);
        qview.sortDescriptions.push(sort);
        staticviews.grid = new wijmo.grid.FlexGrid(id,{
          columns: queue_columns,
          autoGenerateColumns: false,
          itemsSource: qview,
          itemFormatter: lenderFormatter,
          selectionMode: wijmo.grid.SelectionMode.ListBox,
          loadingRows: staticviews.startRowLoading,
          loadedRows: staticviews.endRowLoading,
          selectionChanged: staticviews.handleSelectionChange,
          showAlternatingRows: true
        });
        staticviews.grid.rowHeaders.columns.push(new wijmo.grid.Column(staticviews.rowSelectionColumn));
        var filter = new wijmo.grid.filter.FlexGridFilter(staticviews.grid);
        staticviews.grid.rows.minSize = 40;

        assignusermaps();
        // assign team map
        assigntasktypemap();
        assignlendersmap();
        assignqueuemap();

        brokerengine.staticviews.endRowLoading();
      };

      function lenderFormatter(panel, r, c, cell){
        if(panel.rows[r] instanceof wijmo.grid.detail.DetailRow)
        return;

        if(panel.cellType == wijmo.grid.CellType.TopLeft || panel.cellType == wijmo.grid.CellType.RowHeader)
        {
          staticviews.headerFormatter(panel, r, c, cell);
        }
        if (panel.cellType == wijmo.grid.CellType.Cell && !(panel.rows[r] instanceof wijmo.grid.detail.DetailRow))
        {
          var col = panel.columns[c];
          switch (col.name)
          {
            case 'Lender':
            if(panel.rows[r].dataItem["_mps_lender_value"])
            {
              var lid = panel.rows[r].dataItem["_mps_lender_value"];
              var lobj = panel.grid.columns[c].dataMap.getDisplayValue(lid);
              if(lobj.imgpath)
              {
                cell.innerHTML = "<img src='" + lobj.imgpath + "' alt='" + lobj.name + "' height='32' width='32'>";
                cell.innerHTML += "<div role='button' class='wj-elem-dropdown'><span class='wj-glyph-down'></span></div>";
              }
            }
            break;
          }
        }
      }

      function assignusermaps(){
        var url = brokerengine.odataurl;
        var keymap = [];
        $.getJSON(url + "/systemusers?$select=fullname", function(data){
          var col = staticviews.grid.columns.getColumn("Broker");
          col.dataMap = new wijmo.grid.DataMap(data.value, "systemuserid", "fullname");
          var col = staticviews.grid.columns.getColumn("Worked By");
          col.dataMap = new wijmo.grid.DataMap(data.value, "systemuserid", "fullname");
        });
      }

      function assigntasktypemap(){
        var url = brokerengine.odataurl;
        var keymap = [];
        $.getJSON(url + "/mps_tasktypeses?$select=mps_name", function(data){
          var col = staticviews.grid.columns.getColumn("Task Type");
          col.dataMap = new wijmo.grid.DataMap(data.value, "mps_tasktypesid", "mps_name");
        });
      }

      function assignlendersmap(){
        var url = brokerengine.odataurl;
        var keymap = [];
        $.getJSON(url + "/mps_lenderses?$select=mps_lenders1,entityimage_url", function(data){
          data.value.forEach(function(lender){
            keymap.push({mps_lendersid: lender.mps_lendersid, lender: new brokerengine.entities.Lender(lender.mps_lendersid, lender.mps_lenders1, lender.entityimage_url)});
          });
          var col = staticviews.grid.columns.getColumn("Lender");
          col.dataMap = new wijmo.grid.DataMap(keymap, "mps_lendersid", "lender");
        });
      }

      function assignqueuemap(){
        var url = brokerengine.odataurl;
        var keymap = [];
        $.getJSON(url + "/queues?$select=description", function(data){
          var col = staticviews.grid.columns.getColumn("Queue");
          col.dataMap = new wijmo.grid.DataMap(data.value, "queueid", "description");
        });
      }



    })(staticviews.queues = staticviews.queues || {});

  })(brokerengine.staticviews = brokerengine.staticviews || {});
})(window.brokerengine = window.brokerengine || {}, jQuery);
