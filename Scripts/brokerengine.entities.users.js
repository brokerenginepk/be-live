(function(brokerengine, $, undefined)
{
  (function(entities){
    function User()
    {
      this.systemuserid = "";
      this.fullname = "";
    }
    entities.User = User;

    (function(users){
      var entityName = "systemusers";
      var entityUrl = brokerengine.odataurl + "/" + entityName;
      users.dataMap = {};

      users.assignColumnMap = function(grid, colname)
      {
        var col = grid.columns.getColumn(colname);
        if($.isEmptyObject(users.dataMap))
        {
          var keymap = users.fetch();
          users.dataMap = new wijmo.grid.DataMap(keymap, "systemuserid", "fullname");
        }
        col.dataMap = users.dataMap;
      }

      users.fetch = function(){
        var keymap = [];
        var url = entityUrl + "?$select=fullname";

        if(!$.isEmptyObject(users.dataMap))
        {
          keymap = users.dataMap.collectionView.items;
        }
        else {
          $.ajax({
            async: false,
            dataType: "json",
            url: url,
            success: function(data)
            {
              keymap = data.value;
              /*
              data.value.forEach(function(user){
              keymap.push({systemuserid: user.systemuserid, fullname: user.fullname });
            });
            */
          }
        });
      }
      return keymap;
    }
  })(entities.users = entities.users || {});

})(brokerengine.entities = brokerengine.entities || {});

})(window.brokerengine = window.brokerengine || {}, jQuery);
