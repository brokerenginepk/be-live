(function(brokerengine, $, undefined)
{
  (function(entities){
    function Team()
    {
      this.teamid = "";
      this.name = "";
    }
    entities.Team = Team;

    (function(teams){
      var entityName = "teams";
      var entityUrl = brokerengine.odataurl + "/" + entityName;
      teams.dataMap = {};

      teams.assignColumnMap = function(grid, colname)
      {
        var col = grid.columns.getColumn(colname);
        if($.isEmptyObject(teams.dataMap))
        {
          var keymap = teams.fetch();
          teams.dataMap = new wijmo.grid.DataMap(keymap, "teamid", "name");
        }
        col.dataMap = teams.dataMap;
      }

      teams.fetch = function(){
        var keymap = [];
        var url = entityUrl + "?$select=name&$filter=teamtype eq 0";

        if(!$.isEmptyObject(teams.dataMap))
        {
          keymap = teams.dataMap.collectionView.items;
        }
        else {
          $.ajax({
            async: false,
            dataType: "json",
            url: url,
            success: function(data)
            {
              keymap = data.value;
            }
          });
        }
        return keymap;
      }

      teams.fetchAccessTeams = function(onSuccess){
        /*
          TeamType:
            0 => owner
            1 => access
        */
        var url = entityUrl + "?$select=name&$filter=teamtype eq 1";
          $.ajax({
            dataType: "json",
            url: url
          })
            .done(function(data){
              onSuccess(data.value);
            });

      }
    })(entities.teams = entities.teams || {});

  })(brokerengine.entities = brokerengine.entities || {});

})(window.brokerengine = window.brokerengine || {}, jQuery);
