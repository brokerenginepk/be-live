(function(brokerengine, $, undefined){
  (function(entities){
    function Application(){
    }
    entities.Application = Application;

    (function(applications){
      var entityName = "mps_applications";
      var entityUrl = brokerengine.odataurl + "/" + entityName;
      var activefilter = "$filter=statecode eq 0";
      applications.dataMap = {};

      applications.columns = [
        { binding: "mps_name", name: "Name", header: "Name", width: "8*", minWidth: 200, isReadOnly: true },
        { binding: "_mps_contact_value", name: "Contact", header: "Contact", width: "5*", minWidth: 125, isReadOnly: true },
        { binding: "_bbb_primarysecurityproperty_value", name: "Security Property", header: "Security Property", width: "5*", minWidth: 125, isReadOnly: true },
        { binding: "_mps_lender_value", name: "Lender", header: "Lender", width: "5*", minWidth: 80, isReadOnly: true },
        { binding: "mps_lenderreference", name: "Lender Ref", header: "Lender Ref", width: "5*", minWidth: 125, isReadOnly: true  },
        { binding: "_mps_applicationstage_value", name: "Application Stage", header: "Application Stage", width: "5*", minWidth: 125, isReadOnly: false, isRequired: true },
        { binding: "mps_loanamount", name: "Loan Amount", header: "Loan Amount", format: "c0", width: "5*", minWidth: 125, isReadOnly: true  },
        // Use format: p2 for percentage format. When displaying the value will be multiplied by 100.
        // { binding: "mps_FundingPositionNew.mps_postcaplvr_base", name: "Post Cap LVR", header: "Post Cap LVR", width: "5*", minWidth: 125, isReadOnly: true  },
        { binding: "bbb_postcaplvr", name: "Post Cap LVR", header: "Post Cap LVR", width: "5*", minWidth: 125, format: "p2", isReadOnly: true  },
        { binding: "mps_financedate", name: "Finance Date", header: "Finance Date", width: "5*", minWidth: 125, dataType: wijmo.DataType.Date, format: "dd-MM-yyyy", isRequired: false },
        { binding: "mps_settlementdate", name: "Settlement Date", header: "Settlement Date", width: "5*", minWidth: 125, dataType: wijmo.DataType.Date, format: "dd-MM-yyyy", isRequired: false },
        { binding: "mps_stageduedate", name: "Stage Due Date", header: "Stage Due Date", width: "5*", minWidth: 125, dataType: wijmo.DataType.Date, format: "dd-MM-yyyy", isRequired: false },
        { binding: "_mps_managingteam_value", name: "Assigned Team", header: "Assigned Team", width: "5*", minWidth: 125, isReadOnly: false, isRequired: false },
        { binding: "_ownerid_value", name: "Owner", header: "Broker", width: "5*", minWidth: 125, isReadOnly: true  },
        //{ binding: "mps_FundingPositionNew.mps_fundingtemplateused", name: "Funding Template", header: "Funding Template", width: "5*", minWidth: 125, isReadOnly: true  },
        { binding: "bbb_fundingtemplate", name: "Funding Template", header: "Funding Template", width: "5*", minWidth: 125, isReadOnly: true  },
        ///*ns*/ { binding: "_bbb_primarysecurityproperty_value", name: "Purpose", header: "Purpose", width: "5*", minWidth: 125, isReadOnly: true },
        { binding: "mps_loanpurpose", name: "Purpose", header: "Purpose", width: "5*", minWidth: 125, isReadOnly: true },
        { binding: "_mps_referredby_value", name: "Referred By", header: "Referred By", width: "5*", minWidth: 125, isReadOnly: true  },
        { binding: "createdon", name: "Created On", header: "Created On", width: "5*", minWidth: 125, dataType: wijmo.DataType.Date, format: "dd-MM-yyyy hh:mm:ss", isReadOnly: true  },
        { binding: "statecode", name: "Status", header: "Status", width: "5*", minWidth: 125, isReadOnly: true  }
      ];

      applications.key = "mps_applicationid";
      applications.fields = [
        "mps_name",
        "_mps_contact_value",
        "_bbb_primarysecurityproperty_value",
        "_mps_lender_value",
        "mps_lenderreference",
        "_mps_applicationstage_value",
        "mps_loanamount",
        "mps_financedate",
        "mps_settlementdate",
        "mps_stageduedate",
        "_ownerid_value",
        "_mps_managingteam_value",
        "_mps_fundingpositionnew_value",
        "bbb_postcaplvr",
        "bbb_fundingtemplate",
        "mps_loanpurpose",
        "_mps_referredby_value",
        "createdon",
        "statecode",
        // The following field is added for sharing functionality
        "_mps_brokerlenderdetails_value",
        "_mps_lead_value",
        "_mps_contactforaccess_value",
        "_mps_solicitordetails_value",
        "_bbb_stagedurationhistory_value"
      ];

      applications.field_datatypes = {
        mps_financedate: wijmo.DataType.Date ,
        mps_settlementdate: wijmo.DataType.Date ,
        mps_stageduedate: wijmo.DataType.Date ,
        createdon: wijmo.DataType.Date
      };

      applications.lookups = [
        "mps_FundingPositionNew",
        //				"bbb_PrimarySecurityProperty",
        //				"mps_ManagingTeam"
      ];

      applications.generateGrid = function(id, urlfilter, odata, viewfilter)
      {
        var view = {};
        if(odata)
        {
          var url = brokerengine.odataurl;
          view = new wijmo.odata.ODataCollectionView(url, entityName, {
            fields: applications.fields,
            dataTypes: applications.field_datatypes,
            //lookups: applications.lookups,
            keys: [applications.key],
            filter: viewfilter,
            oDataVersion: 4.0,
            sortOnServer: true,
            filterOnServer: false,
            loading: brokerengine.staticviews.startRowLoading,
            loaded: brokerengine.staticviews.endRowLoading
          });
          view.sortDescriptions.push(new wijmo.collections.SortDescription("mps_name", true));
        }
        else
        {
          var app_arr = brokerengine.entities.applications.fetchApplications(urlfilter);
          view = new wijmo.collections.CollectionView(app_arr);
        }
        var grid = new wijmo.grid.FlexGrid(id, {
          columns: brokerengine.entities.applications.columns,
          autoGenerateColumns: false,
          itemsSource: view,
          itemFormatter: itemFormatter,
          selectionMode: wijmo.grid.SelectionMode.ListBox,
          showAlternatingRows: true,
          allowResizing: wijmo.grid.AllowResizing.Both,
        });

        grid.rows.minSize = 35;
        grid.columnHeaders.rows.defaultSize = 42;
        grid.frozenColumns=1;

        // If data source is OData, set custom editors for date columns. Otherwise make them readonly
        if(odata){
          new CustomGridEditor(grid, 'mps_stageduedate', wijmo.input.InputDate, {
            format: 'dd-MM-yyyy'
          });
          new CustomGridEditor(grid, 'mps_financedate', wijmo.input.InputDate, {
            format: 'dd-MM-yyyy'
          });
          //InterestOnlyExpiryDate
          new CustomGridEditor(grid, 'mps_settlementdate', wijmo.input.InputDate, {
            format: 'dd-MM-yyyy'
          });

            var odatainterface = new brokerengine.ODataInterface(entityName, applications.key);
            new LookupEditor(grid, "_mps_applicationstage_value", "mps_ApplicationStage", "mps_stages", odatainterface);
            new LookupEditor(grid, "_mps_managingteam_value", "mps_ManagingTeam", "teams", odatainterface);
          }
        else{
          grid.columns.getColumn('Application Stage').isReadOnly = true;
          grid.columns.getColumn('Stage Due Date').isReadOnly = true;
          grid.columns.getColumn('Finance Date').isReadOnly = true;
          grid.columns.getColumn('Settlement Date').isReadOnly = true;
          grid.columns.getColumn('Assigned Team').isReadOnly = true;
        }

        var col = grid.columns.getColumn("Status");
        col.dataMap = brokerengine.statusmap;

        brokerengine.entities.securityproperty.assignColumnMap(grid, "Security Property");
        brokerengine.entities.securityproperty.assignColumnMap(grid, "Purpose");
        /*
        var col_purpose = grid.columns.getColumn("Purpose");
        col_purpose.dataMap = new wijmo.grid.DataMap(brokerengine.purpose, "Value", "Label");
        */
        /*
        var col_fundingtemplate = grid.columns.getColumn("Funding Template");
        col_fundingtemplate.dataMap = new wijmo.grid.DataMap(brokerengine.fundingtemplate, "Value", "Label")
        */
        brokerengine.entities.contacts.assignContactsMap(grid, "Contact");
        brokerengine.entities.contacts.assignContactsMap(grid, "Referred By");
        brokerengine.entities.users.assignColumnMap(grid, "Owner");
        brokerengine.entities.teams.assignColumnMap(grid, "Assigned Team");
        brokerengine.entities.lenders.assignColumnMap(grid, "Lender");
        brokerengine.entities.applicationstages.assignColumnMap(grid, "Application Stage");

        return grid;
      }

      applications.fetchById = function(appid, callback, fields)
      {
        var url = entityUrl + "(" + appid + ")";
        url += "?$select="+(fields?fields:applications.fields);

        $.ajax({
          url: url,
          dataType: "json"
        })
          .done(function(app){
            app.mps_financedate = app.mps_financedate?new Date(app.mps_financedate):null;
            app.mps_settlementdate = app.mps_settlementdate?new Date(app.mps_settlementdate):null;
            app.mps_stageduedate = app.mps_stageduedate?new Date(app.mps_stageduedate):null;
            app.createdon = app.createdon?new Date(app.createdon):null;
            callback(app);
          });
      }

      applications.fetchApplications = function(filter)
      {
        var app_arr = [];
        var url = entityUrl + "?$select="+applications.fields;
        //url += "&$expand="+applications.lookups;
        //url += "&"+activefilter;
        if(filter)
        {
          url += " and "+filter;
        }

        $.ajax({
          async: false,
          dataType: "json",
          url: url,
          success: function(data)
          {
            app_arr = data.value;
            app_arr.forEach(function(app){
              app.mps_financedate = app.mps_financedate?new Date(app.mps_financedate):null;
              app.mps_settlementdate = app.mps_settlementdate?new Date(app.mps_settlementdate):null;
              app.mps_stageduedate = app.mps_stageduedate?new Date(app.mps_stageduedate):null;
              app.createdon = app.createdon?new Date(app.createdon):null;
            });
          }
        });
        return app_arr;
      }

      // Still expands the fundingposition lookup. Used by fundingpositions
      applications.fetchApplications2 = function(filter, callback)
      {
        var app_arr = [];
        var url = entityUrl + "?$select="+applications.fields;
        url += "&$expand="+applications.lookups;
        url += "&"+activefilter;
        if(filter)
        {
          url += " and "+filter;
        }

        $.ajax({
          url: url,
          dataType: "json"
        })
          .done(function(data){
            app_arr = data.value;
            app_arr.forEach(function(app){
              app.mps_financedate = app.mps_financedate?new Date(app.mps_financedate):null;
              app.mps_settlementdate = app.mps_settlementdate?new Date(app.mps_settlementdate):null;
              app.mps_stageduedate = app.mps_stageduedate?new Date(app.mps_stageduedate):null;
              app.createdon = app.createdon?new Date(app.createdon):null;
            });
            callback(app_arr);
          });
      }

      function itemFormatter(panel, r, c, cell){
        if(panel.rows[r] instanceof wijmo.grid.detail.DetailRow)
        return;

        var col = panel.columns[c];
        if(panel.cellType == wijmo.grid.CellType.TopLeft || panel.cellType == wijmo.grid.CellType.RowHeader)
        {
          brokerengine.staticviews.headerFormatter(panel, r, c, cell);
        }
        else if (panel.cellType == wijmo.grid.CellType.Cell && !(panel.rows[r] instanceof wijmo.grid.detail.DetailRow))
        {

          // use chartInfo to draw a bar chart
          var col = panel.columns[c];
          var cellhtml;

          // create other types of custom content
          switch (col.name)
          {
            case "Name":
            cell.innerHTML =
            "<a href='../mps_/processdashboardPopup.html?data=" + panel.rows[r].dataItem['mps_applicationid'] + "' target='_blank'>" + panel.rows[r].dataItem['mps_name'] + "</a>";
            break;
            case "Lender":
            cellhtml = brokerengine.entities.lenders.formatCell(panel.rows[r].dataItem, "_mps_lender_value");
            if(cellhtml)
            {
              cell.innerHTML = cellhtml;
            }
            break;

            case "Security Property":
            var id = panel.rows[r].dataItem["_bbb_primarysecurityproperty_value"];
            cellhtml = brokerengine.entities.securityproperty.formatCell(id, "Security Property", true);
            if(cellhtml){
              cell.innerHTML = cellhtml;
            }
            break;

            case "Purpose":
            var id = panel.rows[r].dataItem["_bbb_primarysecurityproperty_value"];
            cellhtml = brokerengine.entities.securityproperty.formatCell(id, "Purpose")
            if(cellhtml){
              cell.innerHTML = cellhtml;
            }
            break;

            case "Finance Date":
            case "Settlement Date":
            case "Stage Due Date":
              if(!col.isReadOnly){
                cell.innerHTML += "<span class='date-edit wj-glyph-pencil'></span>";            
              }
            break;


          }
        }
      }

      applications.assignColumnMap = function(grid, colname)
      {
        var col = grid.columns.getColumn(colname);
        if(!$.isEmptyObject(applications.dataMap))
        {
          col.dataMap = applications.dataMap;
          return;
        }
        else
        {
          var url = entityUrl + "?$select=mps_name";
          brokerengine.getData(url, function(apps){
            brokerengine.entities.applications.dataMap = new wijmo.grid.DataMap(apps, "mps_applicationid", "mps_name");
            col.dataMap = brokerengine.entities.applications.dataMap;            
          });
/*
          $.ajax({
            dataType: "json",
            url: url
          })
            .done(function(data)
            {
              applications.dataMap = new wijmo.grid.DataMap(data.value, "mps_applicationid", "mps_name");
              col.dataMap = applications.dataMap;
            });
*/ 
        }
      }
    })(entities.applications = entities.applications || {});

  })(brokerengine.entities = brokerengine.entities || {});

})(window.brokerengine = window.brokerengine || {}, jQuery);
