(function(brokerengine, $, undefined)
{
	(function(entities){
		function ApplicationStage()
		{
			this.systemuserid = "";
			this.fullname = "";
		}
		entities.ApplicationStage = ApplicationStage;

		(function(stages){
			var entityName = "mps_stages";
			var entityUrl = brokerengine.odataurl + "/" + entityName;
			stages.dataMap = {};

			stages.assignColumnMap = function(grid, colname)
			{
				var col = grid.columns.getColumn(colname);
				if($.isEmptyObject(stages.dataMap))
				{
					var keymap = stages.fetchStages();
					stages.dataMap = new wijmo.grid.DataMap(keymap, "mps_stageid", "mps_stage1");
					stages.dataMap.getDisplayValues = function(item){
						var arr_stages = stages.dataMap.collectionView.items;
						var stagenames = [];
						var curstage = item ? item._mps_applicationstage_value : null;
						arr_stages.forEach(function(stage){
							if(stage.mps_stagetype=="Parent" || stage.mps_stageid==curstage)
							{
								stagenames.push(stage.mps_stage1);
							}
						});
						return stagenames;
					}
				}
				col.dataMap = stages.dataMap;
			}

			stages.fetchStages = function(){
				var keymap = [];
				var url = entityUrl + "?$select=mps_stage1,mps_stagetype";
				url += "&$filter=mps_entitytype%20eq%20922600000%20and%20statecode%20eq%200%20and%20mps_dashboard%20eq%20922600001";
				url += "&$orderby=mps_stage1%20asc";

				if(!$.isEmptyObject(stages.dataMap))
				{
					keymap = stages.dataMap.collectionView.items;
				}
				else {
					$.ajax({
						async: false,
						dataType: "json",
						url: url,
						success: function(data)
						{
							var stagetype = "";
							data.value.forEach(function(stage){
								stagetype = (stage.mps_stagetype==922600000) ? "Parent" : "Child";
								keymap.push({mps_stageid: stage.mps_stageid, mps_stage1: stage.mps_stage1, mps_stagetype: stagetype });
							});
						}
					});
				}
				return keymap;
			}
		})(entities.applicationstages = entities.applicationstages || {});

	})(brokerengine.entities = brokerengine.entities || {});

})(window.brokerengine = window.brokerengine || {}, jQuery);
