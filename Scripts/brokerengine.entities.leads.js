(function(brokerengine, $, undefined)
{
  (function(entities){
    function Lead()
    {
      /*
      this.prefferedname;
      this.name;
      this.leadid;
      */
    }
    entities.Lead = Lead;

    (function(leads){
      var entityName = "leads";
      var entityUrl = brokerengine.odataurl + "/" + entityName;
      var activefilter = "$filter=statecode eq 0";

      //numan
      //var entityUrl = brokerengine.odataurl + "/" + entityName+"?$filter=mps_nickname ne null";
      leads.dataMap = {};

      var columns = [
        //				{ binding: "mps_nickname", name: "Preffered Name", header: "Preffered Name", width: "8*", minWidth: 125 },
        //numan
        { binding: "mps_nickname", name: "Lead Name", header: "Lead Name", width: "8*", minWidth: 125, isReadOnly: true },
        { binding: "_parentcontactid_value", name: "Parent Contact", header: "Parent Contact", width: "8*", minWidth: 125, isReadOnly: true },
        { binding: "parentcontactid.lastname", name: "Last Name", header: "Last Name", width: "8*", minWidth: 125, isReadOnly: true },
        { binding: "parentcontactid.firstname", name: "First Name", header: "First Name", width: "8*", minWidth: 125, isReadOnly: true },
        { binding: "parentcontactid.nickname", name: "Preferred Name", header: "Preferred Name", width: "8*", minWidth: 125, isReadOnly: true },
        { binding: "parentcontactid.mobilephone", name: "Mobile Phone", header: "Mobile Phone", width: "8*", minWidth: 125, isReadOnly: true },
        { binding: "parentcontactid.emailaddress1", name: "Email", header: "Email", width: "8*", minWidth: 125, isReadOnly: true },
        { binding: "parentcontactid.address1_composite", name: "Residential Address", header: "Residential Address", width: "8*", minWidth: 125, isReadOnly: true },
        { binding: "_mps_leadstage_value", name: "Lead Stage", header: "Lead Stage", width: "8*", minWidth: 125, isReadOnly: true },
        { binding: "bbb_loanamount", name: "Loan Amount", header: "Loan Amount", format: "c0", width: "8*", minWidth: 125, isReadOnly: true },
        { binding: "_mps_brand_value", name: "Brand", header: "Brand", width: "8*", minWidth: 125, isReadOnly: true },
        { binding: "parentcontactid._mps_referredby_value", name: "Referred By", header: "Referred By", width: "8*", minWidth: 125, isReadOnly: true },
        { binding: "_ownerid_value", name: "Owner", header: "Broker", width: "8*", minWidth: 125, isReadOnly: true },
        { binding: "_mps_managingteam_value", name: "Managing Team", header: "Managing Team", width: "8*", minWidth: 125, isReadOnly: true },
        { binding: "mps_nextappointmentdate", name: "Appointment", header: "Appointment", width: "8*", minWidth: 125, isReadOnly: true, dataType: wijmo.DataType.Date, format: "dd-MM-yyyy" },
        { binding: "mps_stageduedate", name: "Stage Due Date", header: "Stage Due Date", width: "8*", minWidth: 125, isReadOnly: true, dataType: wijmo.DataType.Date, format: "dd-MM-yyyy" },
        { binding: "statuscode", name: "Status", header: "Status", width: "8*", minWidth: 125, isReadOnly: true }
      ];

      var idfields = [
        "leadid"
      ];
      var fields = [
        "mps_nickname",
        "_parentcontactid_value",
        "_mps_leadstage_value",
        "bbb_loanamount",
        "_mps_brand_value",
        "_bbb_referredby_value",
        "_ownerid_value",
        "_mps_managingteam_value",
        "mps_nextappointmentdate",
        "mps_stageduedate",
        "statuscode"
      ];

      var field_datatypes = {
        mps_nextappointmentdate: wijmo.DataType.Date,
        mps_stageduedate: wijmo.DataType.Date
      };

      var lookups = [
        "parentcontactid"
      ];

      var sortby = new wijmo.collections.SortDescription("mps_nickname", true);

      leads.status = [
        { Label:"New", Value: 1 },
        { Label:"Contacted", Value: 2 },
        { Label:"Qualified", Value: 3 },
        { Label:"Lost", Value: 4 },
        { Label:"Cannot Contact", Value: 5 },
        { Label:"No Longer Interested", Value: 6 },
        { Label:"Canceled", Value: 7 },
      ];

      leads.generateGrid = function(id, urlfilter, odata, viewfilter)
      {
        var view = {};
        if(odata)
        {
          var url = brokerengine.odataurl;
          var view = new wijmo.odata.ODataCollectionView(url, entityName, {
          //var view = new wijmo.odata.ODataCollectionView(url, entityName+"?" + activefilter + " and mps_nickname ne null", {
             fields: fields,
            dataTypes: field_datatypes,
            lookups: lookups,
            keys: idfields,
            filter: viewfilter,
            oDataVersion: 4.0,
            sortOnServer: true,
            filterOnServer: false,
            loading: brokerengine.staticviews.startRowLoading,
            loaded: brokerengine.staticviews.endRowLoading
          });

          view.sortDescriptions.push(sortby);
        }
        else
        {
          var items = brokerengine.entities.leads.fetchLeads(urlfilter);
          view = new wijmo.collections.CollectionView(items);
        }
        var grid = new wijmo.grid.FlexGrid(id, {
          columns: columns,
          autoGenerateColumns: false,
          itemsSource: view,
          itemFormatter: itemFormatter,
          selectionMode: wijmo.grid.SelectionMode.ListBox,
          showAlternatingRows: true
        });
        grid.rows.minSize = 35;
        grid.columnHeaders.rows.defaultSize = 42;
        grid.frozenColumns=1;
        //grid.isReadOnly=true;
        grid.rowHeaders.columns.splice(0, 1);

        brokerengine.entities.contacts.assignContactsMap(grid, "Parent Contact");
        brokerengine.entities.stages.assignColumnMap(grid, "Lead Stage", brokerengine.entities.stages.targetEntity.LEADS);
        brokerengine.entities.brands.assignColumnMap(grid, "Brand");
        brokerengine.entities.contacts.assignContactsMap(grid, "Referred By");
        brokerengine.entities.users.assignColumnMap(grid, "Owner");
        brokerengine.entities.teams.assignColumnMap(grid, "Managing Team");
        brokerengine.entities.leads.assignStatusMap(grid, "Status")

        return grid;
      }

      leads.fetchLeads = function(filter)
      {
        var app_arr = [];
        var url = entityUrl + "?$select="+leads.fields;
        url += "&$expand="+leads.lookups;
        url += "&"+activefilter;
        if(filter)
        {
          url += " and "+filter;
        }

        $.ajax({
          async: false,
          dataType: "json",
          url: url,
          success: function(data)
          {
            app_arr = data.value;
            app_arr.forEach(function(app){
              app.mps_financedate = new Date(app.mps_financedate);
              app.mps_settlementdate = new Date(app.mps_settlementdate);
              app.mps_stageduedate = new Date(app.mps_stageduedate);
              app.createdon = new Date(app.createdon);
            });
          }
        });
        return app_arr;
      }

      function itemFormatter(panel, r, c, cell)
      {
        if(panel.rows[r] instanceof wijmo.grid.detail.DetailRow)
        return;

        var col = panel.columns[c];
        if(panel.cellType == wijmo.grid.CellType.TopLeft || panel.cellType == wijmo.grid.CellType.RowHeader)
        {
          brokerengine.staticviews.headerFormatter(panel, r, c, cell);
        }
        else if (panel.cellType == wijmo.grid.CellType.Cell && !(panel.rows[r] instanceof wijmo.grid.detail.DetailRow))
        {

          // use chartInfo to draw a bar chart
          var col = panel.columns[c];
          var cellhtml;

          // create other types of custom content
          switch (col.name){
            //Numan
            case "Lead Name":
              var name = panel.rows[r].dataItem['mps_nickname']?panel.rows[r].dataItem['mps_nickname']:"(no name)";
              cell.innerHTML =
                "<a href='../mps_/leaddashboardPopup.html?data=" + panel.rows[r].dataItem['leadid'] + "' target='_blank'>" + name + "</a>";
            break;
            case "Parent Contact":
              var id = panel.rows[r].dataItem["_parentcontactid_value"];
              if(id)
              {
                var pname = panel.rows[r].dataItem["parentcontactid"].fullname;
                var nick = panel.rows[r].dataItem["parentcontactid"].nickname;
                var name = pname?pname:(nick?nick:"(no name)");
                var contact = new brokerengine.entities.Contact(id, name);
                cell.innerHTML = contact.getItemUrl();
              }
            break;
          }
        }
      }

      leads.assignStatusMap = function(grid, colname)
      {
        var col = grid.columns.getColumn(colname);
        col.dataMap = new wijmo.grid.DataMap(leads.status, "Value", "Label");
      }

      leads.assignLeadsMap = function(grid, colname)
      {
        var col = grid.columns.getColumn(colname);
        if($.isEmptyObject(leads.dataMap))
        {
          var url = entityUrl + "?$select=mps_nickname";
          $.ajax({
            async: false,
            dataType: "json",
            url: url,
            success: function(data)
            {
              leads.dataMap = new wijmo.grid.DataMap(data.value, "leadid", "mps_nickname");
            }
          });
        }
        col.dataMap = leads.dataMap;
      }
    })(entities.leads = entities.leads || {});

  })(brokerengine.entities = brokerengine.entities || {});

})(window.brokerengine = window.brokerengine || {}, jQuery);
