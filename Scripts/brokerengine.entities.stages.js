(function(brokerengine, $, undefined)
{
	(function(entities){
		function Stage()
		{
			this.stageid = "";
			this.name = "";
		}
		entities.Stage = Stage;

		(function(stages){
			var entityName = "mps_stages";
			var entityUrl = brokerengine.odataurl + "/" + entityName;
			stages.dataMap = {};

			stages.targetEntity = {
				APPS:		922600000,
				CONTACTS:	922600001,
				LEADS:		922600002,
				TODO:		922600003
			};

			stages.assignColumnMap = function(grid, colname, target)
			{
				var col = grid.columns.getColumn(colname);
				if($.isEmptyObject(stages.dataMap))
				{
					var keymap = stages.fetchStages(target);
					stages.dataMap = new wijmo.grid.DataMap(keymap, "mps_stageid", "mps_stage1");
				}
				col.dataMap = stages.dataMap;
			}

			stages.fetchStages = function(target){
				var keymap = [];
				var url = entityUrl + "?$select=mps_stage1";
				url += "&$filter=mps_entitytype%20eq%20" + target;
				url += "%20and%20statecode%20eq%200";

				if(!$.isEmptyObject(stages.dataMap))
				{
					keymap = stages.dataMap.collectionView.items;
				}
				else {
					$.ajax({
						async: false,
						dataType: "json",
						url: url,
						success: function(data)
						{
							data.value.forEach(function(stage){
								keymap.push({mps_stageid: stage.mps_stageid, mps_stage1: stage.mps_stage1 });
							});
						}
					});
				}
				return keymap;
			}
		})(entities.stages = entities.stages || {});

	})(brokerengine.entities = brokerengine.entities || {});

})(window.brokerengine = window.brokerengine || {}, jQuery);
