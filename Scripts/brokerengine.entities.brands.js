(function(brokerengine, $, undefined)
{
	(function(entities){
		function Brand()
		{
			this.brandid = "";
			this.name = "";
		}
		entities.Brand = Brand;

		(function(brands){
			var entityName = "mps_brands";
			var entityUrl = brokerengine.odataurl + "/" + entityName;
			brands.dataMap = {};

			brands.assignColumnMap = function(grid, colname)
			{
				var col = grid.columns.getColumn(colname);
				if(!$.isEmptyObject(brands.dataMap))
				{
					col.dataMap = brands.dataMap;
					return;
				}
				else
				{
					var url = entityUrl + "?$select=mps_name";

					$.ajax({
						dataType: "json",
						url: url,
						success: function(data)
						{
							brands.dataMap = new wijmo.grid.DataMap(data.value, "mps_brandid", "mps_name");
							col.dataMap = brands.dataMap;
						}
					});
				}
			}
		})(entities.brands = entities.brands || {});

	})(brokerengine.entities = brokerengine.entities || {});

})(window.brokerengine = window.brokerengine || {}, jQuery);
