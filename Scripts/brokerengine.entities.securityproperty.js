(function(brokerengine, $, undefined) {
  (function(entities) {

    function SecurityProperty(sp) {
      this.id = sp.mps_securitypropertyid;
      this.name = sp.mps_securityproperty1;
      this.purpose_val = sp.mps_purpose;
      if(sp.mps_purpose) {
        this.purpose = brokerengine.purposemap.getDisplayValue(this.purpose_val);
      }

      SecurityProperty.prototype.toString = function() {
        return this.name;
      }

      SecurityProperty.prototype.toLowerCase = function() {
        return this.name.toLowerCase();
      }
    }
    entities.SecurityProperty = SecurityProperty;

    (function(securityproperty){
      var entityName = "mps_securityproperties";
      var entityUrl = brokerengine.odataurl + "/" + entityName;
      securityproperty.dataMap = {};

      securityproperty.assignColumnMap = function(grid, col)
      {
        var col = grid.columns.getColumn(col);
        if($.isEmptyObject(securityproperty.dataMap))
        {
          var keymap = securityproperty.fetch();
          securityproperty.dataMap = new wijmo.grid.DataMap(keymap, "id", "securityproperty");
        }
        col.dataMap = securityproperty.dataMap;
      }

      securityproperty.fetch = function()
      {
        var keymap = [];
        var url = entityUrl + "?$select=mps_securityproperty1,mps_purpose";
        if(!$.isEmptyObject(securityproperty.dataMap))
        {
          keymap = securityproperty.dataMap.collectionView.items;
        }
        else {
          $.ajax({
            async: false,
            dataType: "json",
            url: url,
            success: function(data)
            {
              data.value.forEach(function(rec){
                keymap.push({id: rec.mps_securitypropertyid, securityproperty: new SecurityProperty(rec) });
              });
            }
          });
        }
        return keymap;
      }

      securityproperty.formatCell = function(id, colname, readonly)
      {
        var html = "";
        if(!id || $.isEmptyObject(securityproperty.dataMap))
        {
          return html;
        }
        var obj = securityproperty.dataMap.getDisplayValue(id);
        switch(colname)
        {
          case "Purpose":
          html = obj.purpose;
          break;
          default:
          html = obj.name;
          if(!readonly)
          {
            html += "<div role='button' class='wj-elem-dropdown'><span class='wj-glyph-down'></span></div>";
          }
          break;
        }
        return html;
      }

    })(entities.securityproperty = entities.securityproperty || {});

  })(brokerengine.entities = brokerengine.entities || {});

})(window.brokerengine = window.brokerengine || {}, jQuery);
