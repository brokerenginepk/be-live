(function(brokerengine, $, undefined)
{
  (function(entities){
    (function(fundingpositions){
      var entityName = "mps_fundingpositionnews";
      var entityUrl = brokerengine.odataurl + "/" + entityName;
      var activefilter = "$filter=statecode eq 0";
      fundingpositions.dataMap = {};
      function Split()
      {
        this.Contact = "";
        this.ApplicationId = "";
        this.Appliation = "";
        this.SecurityProperty = "";
        this.Lender = "";
        this.LoanAmount = "";
        this.PostCapLVR = "";
        this.ProductType = "";
        this.LoanPurpose = "";
        this.InterestRate = "";
        this.PricingDiscount = "";
        this.NetRate = "";
        this.PMTOptions = "";
        this.SettlementDate = "";
        this.FixedRateExpiryDate = "";
        this.InterestOnlyExpiryDate = "";
        // this.ClawbackDate ="";
        this.LoanBSB = "";
        this.LoanAccNumber = "";
      }
      fundingpositions.Split = Split;

      fundingpositions.columns = [
        { binding: "Contact", name: "Contact", header: "Contact", width: "8*", minWidth: 125, isReadOnly: true },
        { binding: "Application", name: "Deal", header: "Deal", width: "8*", minWidth: 200, isReadOnly: true },
        { binding: "SecurityProperty", name: "Security Property", header: "Security Property", width: "8*", minWidth: 150, isReadOnly: true },
        { binding: "Lender", name: "Lender", header: "Lender", width: "5*", minWidth: 80, isReadOnly: true },
        { binding: "LoanAmount", name: "Loan Amount", header: "Loan Amount", format: "c0", width: "5*", minWidth: 125, isReadOnly: true },
        { binding: "PostCapLVR", name: "Post Cap LVR", header: "Post Cap LVR", width: "5*", minWidth: 125, isReadOnly: true },
        { binding: "ProductType", name: "Product Type", header: "Product Type", width: "5*", minWidth: 125, isReadOnly: true },
        { binding: "LoanPurpose", name: "Loan Purpose", header: "Loan Purpose", width: "5*", minWidth: 125, isReadOnly: true },
        { binding: "InterestRate", name: "Interest Rate", header: "Interest Rate", width: "5*", minWidth: 125, isReadOnly: true, format: "n2" },
        { binding: "PricingDiscount", name: "Pricing Discount", header: "Pricing Discount", width: "5*", minWidth: 125, isReadOnly: true, format: "n2" },
        { binding: "NetRate", name: "Net Rate", header: "Net Rate", width: "5*", minWidth: 200, isReadOnly: true, format: "n2" },
        { binding: "ChangeDirection", name: "Change Direction", header: "Change Direction", width: 25, minWidth: 25, isReadOnly: true, format: "n2" },
        { binding: "RateChangeDate", name: "Rate Change Date", header: "Rate Change Date", width: 25, minWidth: 25, dataType: wijmo.DataType.Date, format: "dd-MM-yyyy", isReadOnly: true },
        { binding: "PMTOptions", name: "PMT Options", header: "PMT Options", width: "5*", minWidth: 125, isReadOnly: true },
        { binding: "SettlementDate", name: "Settlement Date", header: "Settlement Date", width: "5*", minWidth: 125, dataType: wijmo.DataType.Date, format: "dd-MM-yyyy", isReadOnly: true },
        { binding: "FixedRateExpiryDate", name: "Fixed Rate Expiry Date", header: "Fixed Rate Expiry Date", width: "5*", minWidth: 125, dataType: wijmo.DataType.Date, format: "dd-MM-yyyy", isReadOnly: true },
        { binding: "InterestOnlyExpiryDate", name: "Interest Only Expiry Date", header: "Interest Only Expiry Date", width: "5*", minWidth: 125, dataType: wijmo.DataType.Date, format: "dd-MM-yyyy", isReadOnly: true },
        { binding: "ClawbackDate", name: "Clawback Date", header: "Clawback Date", width: "5*", minWidth: 125, dataType: wijmo.DataType.Date, format: "dd-MM-yyyy", isReadOnly:true },
        { binding: "LoanBSB", name: "Loan BSB", header: "Loan BSB", width: "5*", minWidth: 125 },
        { binding: "LoanAccNumber", name: "Loan Account Number", header: "Loan Account Number", width: "5*", minWidth: 125 },
        { binding: "Owner", name: "Owner", header: "Broker", width: "5*", minWidth: 125 },
      ];

      fundingpositions.fields = [
        "bbb_clawbackexpires",
        "_mps_contact_value",
        "_mps_application_value",
        "_mps_application_value",
        "_bbb_primarysecurityproperty_value",
        "_mps_lender_value",
        "mps_postcaplvr",
        "bbb_settlementdate",
        "_ownerid_value",

        "mps_loanamount1",
        "mps_producttypeloantype1",
        "mps_loanpurposesplit1",
        "mps_interestrate1",
        "mps_pricingdiscount1",
        "mps_netrate1",
        "mps_pmtoption1",
        "mps_fixedrateexpirydate1",
        "mps_interestonlyexpirydate1",
        "mps_loanbsb1",
        "mps_loanaccountnumber1",

        "mps_loanamount2",
        "mps_producttypeloantype2",
        "mps_loanpurposesplit2",
        "mps_interestrate2",
        "mps_pricingdiscount2",
        "mps_netrate2",
        "mps_pmtoption2",
        "mps_fixedrateexpirydate2",
        "mps_interestonlyexpirydate2",
        "mps_loanbsb2",
        "mps_loanaccountnumber2",

        "mps_loanamount3",
        "mps_producttypeloantype3",
        "mps_loanpurposesplit3",
        "mps_interestrate3",
        "mps_pricingdiscount3",
        "mps_netrate3",
        "mps_pmtoption3",
        "mps_fixedrateexpirydate3",
        "mps_interestonlyexpirydate3",
        "mps_loanbsb3",
        "mps_loanaccountnumber3",

        "mps_loanamount4",
        "mps_producttypeloantype4",
        "mps_loanpurposesplit4",
        "mps_interestrate4",
        "mps_pricingdiscount4",
        "mps_netrate4",
        "mps_pmtoption4",
        "mps_fixedrateexpirydate4",
        "mps_interestonlyexpirydate4",
        "mps_loanbsb4",
        "mps_loanaccountnumber4",

        "mps_loanamount5",
        "mps_producttypeloantype5",
        "mps_loanpurposesplit5",
        "mps_interestrate5",
        "mps_pricingdiscount5",
        "mps_netrate5",
        "mps_pmtoption5",
        "mps_fixedrateexpirydate5",
        "mps_interestonlyexpirydate5",
        "mps_loanbsb5",
        "mps_loanaccountnumber5",

        "bbb_ratechangedate1",
        "bbb_ratechangedate2",
        "bbb_ratechangedate3",
        "bbb_ratechangedate4",
        "bbb_ratechangedate5",

        "bbb_ratechangedifference1",
        "bbb_ratechangedifference2",
        "bbb_ratechangedifference3",
        "bbb_ratechangedifference4",
        "bbb_ratechangedifference5",

        "bbb_ratechangedirection1",
        "bbb_ratechangedirection2",
        "bbb_ratechangedirection3",
        "bbb_ratechangedirection4",
        "bbb_ratechangedirection5",

        "bbb_istrackingrate1",
        "bbb_istrackingrate2",
        "bbb_istrackingrate3",
        "bbb_istrackingrate4",
        "bbb_istrackingrate5",
      ];

      fundingpositions.generateSplitsGrid = function(id, filter, odata)
      {
        brokerengine.staticviews.startRowLoading();

        var view = {};
        var grid = {};
        var splitsarr = [];
        if(odata){
        } else{
          var app_arr = [];
          //brokerengine.entities.applications.fetchApplications2(filter, function(apps){
          brokerengine.entities.fundingpositions.fetchSplits(filter, function(splits){
            splits.forEach(function(split){
              splitsarr.push(split)
            });
            view.refresh();
            grid.refresh();
            brokerengine.staticviews.endRowLoading();
            brokerengine.staticviews.grid.autoSizeRows();
          });

          view = new wijmo.collections.CollectionView(splitsarr);
        }
        view.sortDescriptions.push(new wijmo.collections.SortDescription("Contact", true));
        grid = new wijmo.grid.FlexGrid(id, {
          columns: brokerengine.entities.fundingpositions.columns,
          autoGenerateColumns: false,
          itemsSource: view,
          itemFormatter: itemFormatter,
          selectionMode: wijmo.grid.SelectionMode.ListBox,
          showAlternatingRows: true
        });
        // grid.rows.minSize = 40;
        /*ns*/
        grid.rows.minSize = 35;
        grid.columnHeaders.rows.defaultSize = 42;
        grid.frozenColumns=2;

        var col = {};
        brokerengine.entities.applications.assignColumnMap(grid, "Application");
        brokerengine.entities.securityproperty.assignColumnMap(grid, "Security Property");
        brokerengine.entities.contacts.assignContactsMap(grid, "Contact");
        brokerengine.entities.lenders.assignColumnMap(grid, "Lender");
        brokerengine.entities.users.assignColumnMap(grid, "Owner");

        col = grid.columns.getColumn("Product Type");
        col.dataMap = new wijmo.grid.DataMap(brokerengine.producttype, "Value", "Label");
        col = grid.columns.getColumn("Loan Purpose");
        col.dataMap = new wijmo.grid.DataMap(brokerengine.purpose, "Value", "Label");
        col = grid.columns.getColumn("PMT Options");
        col.dataMap = new wijmo.grid.DataMap(brokerengine.pmtoptions, "Value", "Label");

        return grid;
      }

      fundingpositions.fetchSplits = function(filter, callback)
      {
        var splits_arr = [];
        var url = entityUrl + "?$select="+fundingpositions.fields;
        url += "&"+activefilter;
        if(filter)
        {
          url += " and "+filter;
        }

        $.ajax({
          url: url,
          dataType: "json"
        })
          .done(function(data){
            var fp_arr = data.value;
            var fp_splits;
            fp_arr.forEach(function(fp){
              fp_splits = getSplits(fp);

              fp_splits.forEach(function(split){
                splits_arr.push(split);
              });
            });
            callback(splits_arr);
          });
      }

      fundingpositions.fetchDefaultSplits = function(app, callback)
      {
        var splits_arr = [];
        if(!app._mps_fundingpositionnew_value)
        {
          return splits_arr;
        }

        var url = entityUrl + "(" + app._mps_fundingpositionnew_value + ")";
        $.ajax({
          url: url,
          dataType: "json"
        })
          .done(function(data){
            splits_arr = getAppSplits(app, data);
            callback(splits_arr);
          });
      }

      function itemFormatter(panel, r, c, cell){
        if(panel.rows[r] instanceof wijmo.grid.detail.DetailRow)
        return;

        var col = panel.columns[c];
        if(panel.cellType == wijmo.grid.CellType.TopLeft || panel.cellType == wijmo.grid.CellType.RowHeader){
          brokerengine.staticviews.headerFormatter(panel, r, c, cell);
        }
        else if(panel.cellType == wijmo.grid.CellType.ColumnHeader){
          // Fix text alignment for column headers
          cell.style.textAlign = "";
          var tooltip = new wijmo.Tooltip();
          // Fixes column changes due to scrolling
          cell.style.borderRight = "";
          var col = panel.columns[c];
          switch(col.name){
            case "Net Rate":
              cell.style.borderRight = 0;
              tooltip.setTooltip(cell, "Net Rate")
            break;
            case "Change Direction":
              cell.innerHTML = "";
              cell.style.borderRight = 0;
              tooltip.setTooltip(cell, "Rate Change Direction")
            break;
            case "Rate Change Date":
              cell.innerHTML = "";
              tooltip.setTooltip(cell, "Rate Changed On")
            break;
            default:
            break;
          }
          tooltip = null;
        }
        else if (panel.cellType == wijmo.grid.CellType.Cell && !(panel.rows[r] instanceof wijmo.grid.detail.DetailRow)){
          var col = panel.columns[c];
          // Fixes column changes due to scrolling
          cell.style.borderRight = "";
          switch (col.name){
            case 'Deal':
              var id = panel.rows[r].dataItem['ApplicationId'];
              if(id)
              {
                var name = panel.grid.columns[c].dataMap.getDisplayValue(id);
                if(name == id){
                  name = "( unknown deal )";
                  cell.innerHTML = name;
                }
                else{
                  cell.innerHTML =
                  "<a href='../mps_/processdashboardPopup.html?data=" + id + "' target='_blank'>" + name + "</a>";
                  }
              }
              break;
            case 'Contact':
              if(panel.rows[r].dataItem["Contact"]) {
                var id = panel.rows[r].dataItem["Contact"];
                var name = panel.grid.columns[c].dataMap.getDisplayValue(id);
                var contact = new brokerengine.entities.Contact(id, name);
                cell.innerHTML = contact.getItemUrl();
              }
              break;
            case 'Lender':
            var lenderhtml = brokerengine.entities.lenders.formatCell(panel.rows[r].dataItem, "Lender");
            if(lenderhtml){
              cell.innerHTML = lenderhtml;
            }
            break;
            case "Security Property":
            var id = panel.rows[r].dataItem["SecurityProperty"];
            var cellhtml = brokerengine.entities.securityproperty.formatCell(id, "Security Property", true)
            if(cellhtml){
              cell.innerHTML = cellhtml;
            }
            break;
            case "Interest Rate":
            case "Pricing Discount":
            if(cell.innerHTML){
              cell.innerHTML += "%";
            }
            break;
            case "Net Rate":
              var netrate = panel.rows[r].dataItem["NetRate"];
              var diff = panel.rows[r].dataItem["RateDifference"];
              var changedate = panel.rows[r].dataItem["RateChangeDate"];
              var istracking = panel.rows[r].dataItem["IsTrackingRate"];
              var iconclass = "";
              var iconstyle = "";
              var rateclass = "";
              var addlinfo = "";
              if(diff){
                if(diff > 0){
                  if(istracking){
                    iconclass = "wj-glyph-up be_rate_up";
                    iconstyle = "zoom: 150%;";
                  }
                  else{
                    iconclass = "glyphicon glyphicon-eject be_rate_up";
                    iconstyle = "zoom: 115%;";
                  }
                  rateclass = "be_rate_up";
                }
                else{
                  if(istracking){
                    iconclass = "wj-glyph-down be_rate_down";
                    iconstyle = "zoom: 150%;";
                  }
                  else{
                    iconclass = "glyphicon glyphicon-eject be_rate_down";
                    iconstyle = "transform: rotate(180deg); zoom: 115%";
                  }
                  rateclass = "be_rate_down";
                }

                addlinfo = "&nbsp;<span class='" + rateclass + "'>(" + diff.format("n2") + "%)</span>&nbsp;";
                addlinfo += changedate.format("dd-MM-yyyy");
              }
              else{
                  if(istracking){
                    iconclass = "wj-glyph-square";
                    iconstyle = "zoom: 175%;";
                  }
                  else{
                    iconclass = "glyphicon glyphicon-pause";
                    iconstyle = "zoom: 115%";
                  }
              }

              cell.innerHTML = "<span class='" + iconclass + "' style='" + iconstyle + "'></span>";
              if(netrate){
                cell.innerHTML += "&nbsp;"  + netrate.format("n2") + "%";
              }
              cell.innerHTML += addlinfo;
              cell.style.textAlign = "left";
              cell.style.borderRight = 0;
            break;

            case "Change Direction":
              /*
              var diff = panel.rows[r].dataItem["RateDifference"];
              if(diff){
                var cl = "";
                if(diff > 0){
                  cl = "be_rate_up";
                }
                else{
                  cl = "be_rate_down";
                }
                cell.innerHTML = "<span class='" + cl + "'>(" + cell.innerHTML + ")</span>";;
               }
               */
              cell.innerHTML = "";
              cell.style.borderRight = 0;
            break;

            case "Rate Change Date":
              /*
              var diff = panel.rows[r].dataItem["RateDifference"];
              var changedate = panel.rows[r].dataItem["RateChangeDate"];
              if(diff){
                cell.innerHTML = changedate.format(panel.columns[c].format);
              }
              */
              cell.innerHTML = "";
            break;

            case "Fixed Rate Expiry Date":
            case "Interest Only Expiry Date":
            var cellentry=cell.innerHTML.trim();
            if(cellentry){
              dts=cellentry.split("-");
              var dt = new Date(dts[2], dts[1] - 1, dts[0]);
              var today=new Date();
              var dif=(dt-today)/(3600*1000*24);
              var cl="";
              if(dif<7)
                cl="redish";
              else if (dif<60)
                cl="orangish";
              cell.innerHTML="<span class='date-cell "+cl+"'>"+cellentry+"</span>";
            }
            break;


            // case "Settlement Date":
            // var cellentry=cell.innerHTML.trim();
            // if(cellentry){
            //   dts=cellentry.split("-");
            //   var dt = new Date(dts[2], dts[1] - 1, dts[0]);
            //   var today=new Date();
            //   var dif=(dt-today)/(3600*1000*24);
            //   cl="";
            //   if(dif<0)
            //     cl="redish";
            //   cell.innerHTML="<span class='date-cell "+cl+"'>"+cellentry+"</span>";
            // }
            // break;

            default:
            break;

          }
        }
      }

      function getSplits(fpn)
      {
        var splitsarr = [];
        var split = {};
        //var secprop = application._bbb_primarysecurityproperty_value;
        //var owner = application._ownerid_value;
        var clawbackdate = fpn.bbb_clawbackexpires?new Date(fpn.bbb_clawbackexpires):null;
        if(fpn.mps_loanamount1)
        {
          split = {
            Contact: fpn._mps_contact_value,
            ApplicationId: fpn._mps_application_value,
            Application: fpn._mps_application_value,
            SecurityProperty: fpn._bbb_primarysecurityproperty_value,
            Lender: fpn._mps_lender_value,
            LoanAmount: fpn.mps_loanamount1,
            PostCapLVR: fpn.mps_postcaplvr,
            ProductType: fpn.mps_producttypeloantype1,
            LoanPurpose: fpn.mps_loanpurposesplit1,
            InterestRate: fpn.mps_interestrate1,
            PricingDiscount: fpn.mps_pricingdiscount1,
            NetRate: fpn.mps_netrate1,
            RateDifference: fpn.bbb_ratechangedifference1,
            ChangeDirection: fpn.bbb_ratechangedirection1,
            RateChangeDate: fpn.bbb_ratechangedate1?new Date(fpn.bbb_ratechangedate1):null,
            IsTrackingRate: fpn.bbb_istrackingrate1,
            PMTOptions: fpn.mps_pmtoption1,
            SettlementDate: fpn.bbb_settlementdate?new Date(fpn.bbb_settlementdate):null,
            FixedRateExpiryDate: fpn.mps_fixedrateexpirydate1?new Date(fpn.mps_fixedrateexpirydate1):null,
            InterestOnlyExpiryDate: fpn.mps_interestonlyexpirydate1?new Date(fpn.mps_interestonlyexpirydate1):null,
            ClawbackDate: clawbackdate,
            LoanBSB: fpn.mps_loanbsb1,
            LoanAccNumber: fpn.mps_loanaccountnumber1,
            Owner: fpn._ownerid_value
          };
          splitsarr.push(split);
        }
        if(fpn.mps_loanamount2)
        {
          split = {
            Contact: fpn._mps_contact_value,
            ApplicationId: fpn._mps_application_value,
            Application: fpn._mps_application_value,
            SecurityProperty: fpn._bbb_primarysecurityproperty_value,
            Lender: fpn._mps_lender_value,
            LoanAmount: fpn.mps_loanamount2,
            PostCapLVR: fpn.mps_postcaplvr,
            ProductType: fpn.mps_producttypeloantype2,
            LoanPurpose: fpn.mps_loanpurposesplit2,
            InterestRate: fpn.mps_interestrate2,
            PricingDiscount: fpn.mps_pricingdiscount2,
            NetRate: fpn.mps_netrate2,
            RateDifference: fpn.bbb_ratechangedifference2,
            ChangeDirection: fpn.bbb_ratechangedirection2,
            RateChangeDate: fpn.bbb_ratechangedate2?new Date(fpn.bbb_ratechangedate2):null,
            IsTrackingRate: fpn.bbb_istrackingrate2,
            PMTOptions: fpn.mps_pmtoption2,
            SettlementDate: fpn.mps_settlementdate?new Date(fpn.bbb_settlementdate):null,
            FixedRateExpiryDate: fpn.mps_fixedrateexpirydate2?new Date(fpn.mps_fixedrateexpirydate2):null,
            InterestOnlyExpiryDate: fpn.mps_interestonlyexpirydate2?new Date(fpn.mps_interestonlyexpirydate2) : null,
            ClawbackDate: clawbackdate,
            LoanBSB: fpn.mps_loanbsb2,
            LoanAccNumber: fpn.mps_loanaccountnumber2,
            Owner: fpn._ownerid_value
          };
          splitsarr.push(split);
        }
        if(fpn.mps_loanamount3)
        {
          split = {
            Contact: fpn._mps_contact_value,
            ApplicationId: fpn._mps_application_value,
            Application: fpn._mps_application_value,
            SecurityProperty: fpn._bbb_primarysecurityproperty_value,
            Lender: fpn._mps_lender_value,
            LoanAmount: fpn.mps_loanamount3,
            PostCapLVR: fpn.mps_postcaplvr,
            ProductType: fpn.mps_producttypeloantype3,
            LoanPurpose: fpn.mps_loanpurposesplit3,
            InterestRate: fpn.mps_interestrate3,
            PricingDiscount: fpn.mps_pricingdiscount3,
            NetRate: fpn.mps_netrate3,
            RateDifference: fpn.bbb_ratechangedifference3,
            ChangeDirection: fpn.bbb_ratechangedirection3,
            RateChangeDate: fpn.bbb_ratechangedate3?new Date(fpn.bbb_ratechangedate3):null,
            IsTrackingRate: fpn.bbb_istrackingrate3,
            PMTOptions: fpn.mps_pmtoption3,
            SettlementDate: fpn.bbb_settlementdate?new Date(fpn.bbb_settlementdate):null,
            FixedRateExpiryDate: fpn.mps_fixedrateexpirydate3?new Date(fpn.mps_fixedrateexpirydate3):null,
            InterestOnlyExpiryDate: fpn.mps_interestonlyexpirydate3?new Date(fpn.mps_interestonlyexpirydate3) : null,
            ClawbackDate: clawbackdate,
            LoanBSB: fpn.mps_loanbsb3,
            LoanAccNumber: fpn.mps_loanaccountnumber3,
            Owner: fpn._ownerid_value
          };
          splitsarr.push(split);
        }
        if(fpn.mps_loanamount4)
        {
          split = {
            Contact: fpn._mps_contact_value,
            ApplicationId: fpn._mps_application_value,
            Application: fpn._mps_application_value,
            SecurityProperty: fpn._bbb_primarysecurityproperty_value,
            Lender: fpn._mps_lender_value,
            LoanAmount: fpn.mps_loanamount4,
            PostCapLVR: fpn.mps_postcaplvr,
            ProductType: fpn.mps_producttypeloantype4,
            LoanPurpose: fpn.mps_loanpurposesplit4,
            InterestRate: fpn.mps_interestrate4,
            PricingDiscount: fpn.mps_pricingdiscount4,
            NetRate: fpn.mps_netrate4,
            RateDifference: fpn.bbb_ratechangedifference4,
            ChangeDirection: fpn.bbb_ratechangedirection4,
            RateChangeDate: fpn.bbb_ratechangedate4?new Date(fpn.bbb_ratechangedate4):null,
            IsTrackingRate: fpn.bbb_istrackingrate4,
            PMTOptions: fpn.mps_pmtoption4,
            SettlementDate: fpn.bbb_settlementdate?new Date(fpn.bbb_settlementdate):null,
            FixedRateExpiryDate: fpn.mps_fixedrateexpirydate4?new Date(fpn.mps_fixedrateexpirydate4):null,
            InterestOnlyExpiryDate: fpn.mps_interestonlyexpirydate4?new Date(fpn.mps_interestonlyexpirydate4) : null,
            ClawbackDate: clawbackdate,
            LoanBSB: fpn.mps_loanbsb4,
            LoanAccNumber: fpn.mps_loanaccountnumber4,
            Owner: fpn._ownerid_value
          };
          splitsarr.push(split);
        }
        if(fpn.mps_loanamount5)
        {
          split = {
            Contact: fpn._mps_contact_value,
            ApplicationId: fpn._mps_application_value,
            Application: fpn._mps_application_value,
            SecurityProperty: fpn._bbb_primarysecurityproperty_value,
            Lender: fpn._mps_lender_value,
            LoanAmount: fpn.mps_loanamount5,
            PostCapLVR: fpn.mps_postcaplvr,
            ProductType: fpn.mps_producttypeloantype5,
            LoanPurpose: fpn.mps_loanpurposesplit5,
            InterestRate: fpn.mps_interestrate5,
            PricingDiscount: fpn.mps_pricingdiscount5,
            NetRate: fpn.mps_netrate5,
            RateDifference: fpn.bbb_ratechangedifference5,
            ChangeDirection: fpn.bbb_ratechangedirection5,
            RateChangeDate: fpn.bbb_ratechangedate5?new Date(fpn.bbb_ratechangedate5):null,
            IsTrackingRate: fpn.bbb_istrackingrate5,
            PMTOptions: fpn.mps_pmtoption5,
            SettlementDate: fpn.bbb_settlementdate?new Date(fpn.bbb_settlementdate):null,
            FixedRateExpiryDate: fpn.mps_fixedrateexpirydate5 ? new Date(fpn.mps_fixedrateexpirydate5) : null ,
            InterestOnlyExpiryDate: fpn.mps_interestonlyexpirydate5?new Date(fpn.mps_interestonlyexpirydate5) : null,
            ClawbackDate: clawbackdate,
            LoanBSB: fpn.mps_loanbsb5,
            LoanAccNumber: fpn.mps_loanaccountnumber5,
            Owner: fpn._ownerid_value
          };
          splitsarr.push(split);
        }
        if(fpn.mps_loanamount6)
        {
          split = {
            Contact: fpn._mps_contact_value,
            ApplicationId: fpn._mps_application_value,
            Application: fpn._mps_application_value,
            SecurityProperty: fpn._bbb_primarysecurityproperty_value,
            Lender: fpn._mps_lender_value,
            LoanAmount: fpn.mps_loanamount6,
            PostCapLVR: fpn.mps_postcaplvr,
            ProductType: fpn.mps_producttypeloantype6,
            LoanPurpose: fpn.mps_loanpurposesplit6,
            InterestRate: fpn.mps_interestrate6,
            PricingDiscount: fpn.mps_pricingdiscount6,
            NetRate: fpn.mps_netrate6,
            RateDifference: fpn.bbb_ratechangedifference6,
            ChangeDirection: fpn.bbb_ratechangedirection6,
            RateChangeDate: fpn.bbb_ratechangedate6?new Date(fpn.bbb_ratechangedate6):null,
            IsTrackingRate: fpn.bbb_istrackingrate6,
            PMTOptions: fpn.mps_pmtoption6,
            SettlementDate: fpn.bbb_settlementdate?new Date(fpn.bbb_settlementdate):null,
            FixedRateExpiryDate: fpn.mps_fixedrateexpirydate6 ? new Date(fpn.mps_fixedrateexpirydate6) : null ,
            InterestOnlyExpiryDate: fpn.mps_interestonlyexpirydate6?new Date(fpn.mps_interestonlyexpirydate6) : null,
            ClawbackDate: clawbackdate,
            LoanBSB: fpn.mps_loanbsb6,
            LoanAccNumber: fpn.mps_loanaccountnumber6,
            Owner: fpn._ownerid_value
          };
          splitsarr.push(split);
        }
        return splitsarr;
      }

      function getAppSplits(application, fpn)
      {
        var splitsarr = [];
        var split = {};
        var secprop = application._bbb_primarysecurityproperty_value;
        var owner = application._ownerid_value;
        if(fpn.mps_loanamount1)
        {
          split = {
            Contact: application._mps_contact_value,
            ApplicationId: application.mps_applicationid,
            Application: application.mps_name,
            SecurityProperty: secprop,
            Lender: application._mps_lender_value,
            LoanAmount: fpn.mps_loanamount1,
            PostCapLVR: fpn.mps_postcaplvr,
            ProductType: fpn.mps_producttypeloantype1,
            LoanPurpose: fpn.mps_loanpurposesplit1,
            InterestRate: fpn.mps_interestrate1,
            PricingDiscount: fpn.mps_pricingdiscount1,
            NetRate: fpn.mps_netrate1,
            RateDifference: fpn.bbb_ratechangedifference1,
            ChangeDirection: fpn.bbb_ratechangedirection1,
            RateChangeDate: fpn.bbb_ratechangedate1?new Date(fpn.bbb_ratechangedate1):null,
            IsTrackingRate: fpn.bbb_istrackingrate1,
            PMTOptions: fpn.mps_pmtoption1,
            SettlementDate: application.mps_settlementdate?new Date(mps.bbb_settlementdate):null,
            FixedRateExpiryDate: fpn.mps_fixedrateexpirydate1?new Date(fpn.mps_fixedrateexpirydate1):null,
            InterestOnlyExpiryDate: fpn.mps_interestonlyexpirydate1?new Date(fpn.mps_interestonlyexpirydate1):null,
            // ClawbackDate: fpn.bbb_clawbackexpires1?new Date(fpn.bbb_clawbackexpires1):null,
            LoanBSB: fpn.mps_loanbsb1,
            LoanAccNumber: fpn.mps_loanaccountnumber1,
            Owner: owner
          };
          splitsarr.push(split);
        }
        if(fpn.mps_loanamount2)
        {
          split = {
            Contact: application._mps_contact_value,
            ApplicationId: application.mps_applicationid,
            Application: application.mps_name,
            SecurityProperty: secprop,
            Lender: application._mps_lender_value,
            LoanAmount: fpn.mps_loanamount2,
            PostCapLVR: fpn.mps_postcaplvr,
            ProductType: fpn.mps_producttypeloantype2,
            LoanPurpose: fpn.mps_loanpurposesplit2,
            InterestRate: fpn.mps_interestrate2,
            PricingDiscount: fpn.mps_pricingdiscount2,
            NetRate: fpn.mps_netrate2,
            RateDifference: fpn.bbb_ratechangedifference2,
            ChangeDirection: fpn.bbb_ratechangedirection2,
            RateChangeDate: fpn.bbb_ratechangedate2?new Date(fpn.bbb_ratechangedate2):null,
            IsTrackingRate: fpn.bbb_istrackingrate2,
            PMTOptions: fpn.mps_pmtoption2,
            SettlementDate: application.mps_settlementdate?new Date(mps.bbb_settlementdate):null,
            FixedRateExpiryDate: fpn.mps_fixedrateexpirydate2?new Date(fpn.mps_fixedrateexpirydate2):null,
            InterestOnlyExpiryDate: fpn.mps_interestonlyexpirydate2?new Date(fpn.mps_interestonlyexpirydate2) : null,
            // ClawbackDate: fpn.bbb_clawbackexpires2?new Date(fpn.bbb_clawbackexpires2):null,
            LoanBSB: fpn.mps_loanbsb2,
            LoanAccNumber: fpn.mps_loanaccountnumber2,
            Owner: owner
          };
          splitsarr.push(split);
        }
        if(fpn.mps_loanamount3)
        {
          split = {
            Contact: application._mps_contact_value,
            ApplicationId: application.mps_applicationid,
            Application: application.mps_name,
            SecurityProperty: secprop,
            Lender: application._mps_lender_value,
            LoanAmount: fpn.mps_loanamount3,
            PostCapLVR: fpn.mps_postcaplvr,
            ProductType: fpn.mps_producttypeloantype3,
            LoanPurpose: fpn.mps_loanpurposesplit3,
            InterestRate: fpn.mps_interestrate3,
            PricingDiscount: fpn.mps_pricingdiscount3,
            NetRate: fpn.mps_netrate3,
            RateDifference: fpn.bbb_ratechangedifference3,
            ChangeDirection: fpn.bbb_ratechangedirection3,
            RateChangeDate: fpn.bbb_ratechangedate3?new Date(fpn.bbb_ratechangedate3):null,
            IsTrackingRate: fpn.bbb_istrackingrate3,
            PMTOptions: fpn.mps_pmtoption3,
            SettlementDate: application.mps_settlementdate?new Date(mps.bbb_settlementdate):null,
            FixedRateExpiryDate: fpn.mps_fixedrateexpirydate3?new Date(fpn.mps_fixedrateexpirydate3):null,
            InterestOnlyExpiryDate: fpn.mps_interestonlyexpirydate3?new Date(fpn.mps_interestonlyexpirydate3) : null,
            // ClawbackDate: fpn.bbb_clawbackexpires3?new Date(fpn.bbb_clawbackexpires3):null,
            LoanBSB: fpn.mps_loanbsb3,
            LoanAccNumber: fpn.mps_loanaccountnumber3,
            Owner: owner
          };
          splitsarr.push(split);
        }
        if(fpn.mps_loanamount4)
        {
          split = {
            Contact: application._mps_contact_value,
            ApplicationId: application.mps_applicationid,
            Application: application.mps_name,
            SecurityProperty: secprop,
            Lender: application._mps_lender_value,
            LoanAmount: fpn.mps_loanamount4,
            PostCapLVR: fpn.mps_postcaplvr,
            ProductType: fpn.mps_producttypeloantype4,
            LoanPurpose: fpn.mps_loanpurposesplit4,
            InterestRate: fpn.mps_interestrate4,
            PricingDiscount: fpn.mps_pricingdiscount4,
            NetRate: fpn.mps_netrate4,
            RateDifference: fpn.bbb_ratechangedifference4,
            ChangeDirection: fpn.bbb_ratechangedirection4,
            RateChangeDate: fpn.bbb_ratechangedate4?new Date(fpn.bbb_ratechangedate4):null,
            IsTrackingRate: fpn.bbb_istrackingrate4,
            PMTOptions: fpn.mps_pmtoption4,
            SettlementDate: application.mps_settlementdate?new Date(mps.bbb_settlementdate):null,
            FixedRateExpiryDate: fpn.mps_fixedrateexpirydate4?new Date(fpn.mps_fixedrateexpirydate4):null,
            InterestOnlyExpiryDate: fpn.mps_interestonlyexpirydate4?new Date(fpn.mps_interestonlyexpirydate4) : null,
            // ClawbackDate: fpn.bbb_clawbackexpires4?new Date(fpn.bbb_clawbackexpires4):null,
            LoanBSB: fpn.mps_loanbsb4,
            LoanAccNumber: fpn.mps_loanaccountnumber4,
            Owner: owner
          };
          splitsarr.push(split);
        }
        if(fpn.mps_loanamount5)
        {
          split = {
            Contact: application._mps_contact_value,
            ApplicationId: application.mps_applicationid,
            Application: application.mps_name,
            SecurityProperty: secprop,
            Lender: application._mps_lender_value,
            LoanAmount: fpn.mps_loanamount5,
            PostCapLVR: fpn.mps_postcaplvr,
            ProductType: fpn.mps_producttypeloantype5,
            LoanPurpose: fpn.mps_loanpurposesplit5,
            InterestRate: fpn.mps_interestrate5,
            PricingDiscount: fpn.mps_pricingdiscount5,
            NetRate: fpn.mps_netrate5,
            RateDifference: fpn.bbb_ratechangedifference5,
            ChangeDirection: fpn.bbb_ratechangedirection5,
            RateChangeDate: fpn.bbb_ratechangedate5?new Date(fpn.bbb_ratechangedate5):null,
            IsTrackingRate: fpn.bbb_istrackingrate5,
            PMTOptions: fpn.mps_pmtoption5,
            SettlementDate: application.mps_settlementdate?new Date(mps.bbb_settlementdate):null,
            FixedRateExpiryDate: fpn.mps_fixedrateexpirydate5 ? new Date(fpn.mps_fixedrateexpirydate5) : null ,
            InterestOnlyExpiryDate: fpn.mps_interestonlyexpirydate5?new Date(fpn.mps_interestonlyexpirydate5) : null,
            // ClawbackDate: fpn.bbb_clawbackexpires5?new Date(fpn.bbb_clawbackexpires5):null,
            LoanBSB: fpn.mps_loanbsb5,
            LoanAccNumber: fpn.mps_loanaccountnumber5,
            Owner: owner
          };
          splitsarr.push(split);
        }
        if(fpn.mps_loanamount6)
        {
          split = {
            Contact: application._mps_contact_value,
            ApplicationId: application.mps_applicationid,
            Application: application.mps_name,
            SecurityProperty: secprop,
            Lender: application._mps_lender_value,
            LoanAmount: fpn.mps_loanamount6,
            PostCapLVR: fpn.mps_postcaplvr,
            ProductType: fpn.mps_producttypeloantype6,
            LoanPurpose: fpn.mps_loanpurposesplit6,
            InterestRate: fpn.mps_interestrate6,
            PricingDiscount: fpn.mps_pricingdiscount6,
            NetRate: fpn.mps_netrate6,
            RateDifference: fpn.bbb_ratechangedifference6,
            ChangeDirection: fpn.bbb_ratechangedirection6,
            RateChangeDate: fpn.bbb_ratechangedate6?new Date(fpn.bbb_ratechangedate6):null,
            IsTrackingRate: fpn.bbb_istrackingrate6,
            PMTOptions: fpn.mps_pmtoption6,
            SettlementDate: application.mps_settlementdate?new Date(mps.bbb_settlementdate):null,
            FixedRateExpiryDate: fpn.mps_fixedrateexpirydate6 ? new Date(fpn.mps_fixedrateexpirydate6) : null ,
            InterestOnlyExpiryDate: fpn.mps_interestonlyexpirydate6?new Date(fpn.mps_interestonlyexpirydate6) : null,
            // ClawbackDate: fpn.bbb_clawbackexpires6?new Date(fpn.bbb_clawbackexpires6):null,
            LoanBSB: fpn.mps_loanbsb6,
            LoanAccNumber: fpn.mps_loanaccountnumber6,
            Owner: owner
          };
          splitsarr.push(split);
        }
        return splitsarr;
      }

    })(entities.fundingpositions = entities.fundingpositions || {});

  })(brokerengine.entities = brokerengine.entities || {});

})(window.brokerengine = window.brokerengine || {}, jQuery);
