(function(brokerengine, $, undefined){
  (function(staticviews)  {

    staticviews.rowSelectionColumn = { name: "Is Selected", width: "*", minWidth: 50, dataType: wijmo.DataType.Boolean };
    staticviews.title = "";
    staticviews.grid = {};
    //staticviews.dataviewfilter = {};
    staticviews.filter = {};
    staticviews.showFilteredResults = false;
    staticviews.filterids = [];
    staticviews.selectedItems = [];

    staticviews.handleDeactivateButton=function(deactivateCallback){
      $("#be_staticviews_deactivate").click(deactivateCallback);
    }

    staticviews.handleExportButtons = function(){
      exportOptions = [
        "Export",
        "to PDF: All Rows",
        "to PDF: Selected Rows",
        "to Excel: All Rows",
        "to Excel: Selected Rows",
      ];
      var exportgrid = {};
      exportgrid = new wijmo.input.ComboBox("#export_grid");
      exportgrid.itemsSource = exportOptions;
      exportgrid.isEditable=false;
      exportgrid.selectedIndex=0;
      exportgrid.textChanged.addHandler(function(sv, ev){
        var newview = sv.selectedValue;
        $("#waitLogo").show();
        switch(newview){
          case exportOptions[1]:
            exportpdf("all");
          break;
          case exportOptions[2]:
            exportpdf("selection");
          break;
          case exportOptions[3]:
            exportToExcel("all");
          break;
          case exportOptions[4]:
            exportToExcel("selection");
          break;
        }
        exportgrid.selectedIndex=0;
        exportgrid.invalidate(true);
        $("#waitLogo").hide();
      });
    }



    function exportpdf(datamode){
      if (datamode=="selection" && staticviews.grid.selectedRows.length==0){
        alert("No Data Selected");
        return;
      }


      if($.isEmptyObject(staticviews.grid)){
        alert("Load a view to export");
        return;
      }
      pdf_options={
        scaleMode: wijmo.grid.pdf.ScaleMode.PageWidth,
        styles: {
          cellStyle: {backgroundColor: '#ffffff',borderColor: '#c6c6c6'},
          headerCellStyle: {backgroundColor: '#eaeaea'}
        },
        documentOptions: {info: {title: window.top.displayName}}
      }
      if (datamode=="all")
        pdf_options["exportMode"]= wijmo.grid.pdf.ExportMode.All;
      else if (datamode=="selection")
        pdf_options["exportMode"]= wijmo.grid.pdf.ExportMode.Selection;
      wijmo.grid.pdf.FlexGridPdfConverter.export(staticviews.grid, staticviews.title+".pdf", pdf_options);
    }


    exportToExcel=function (datamode){
      if (datamode=="selection" && staticviews.grid.selectedRows.length==0){
        alert("No Data Selected");
        return;
      }

      if($.isEmptyObject(staticviews.grid)){
        alert("Load a view to export");
        return;
      }

      var exportgrid;
      if(datamode=="all"){
        exportgrid=staticviews.grid;
      } else if (datamode=="selection"){
        var data=[];
        var colmns=[];
        var width=staticviews.grid.cells.columns.length;
        for (var j=0;j<width;j++){
          colmns.push(staticviews.grid.cells.columns[j].header);
        }
        var height=staticviews.grid.cells.rows.length;
        for (var i=0;i<height;i++){
          if(staticviews.grid.rows[i].isSelected){
            var ro={};
            for (j=0;j<width;j++){
              var celldata=staticviews.grid.getCellData(i,j) || "";
              if (staticviews.grid.columns[j].dataMap)
                celldata = staticviews.grid.columns[j].dataMap.getDisplayValue(celldata);
              if (celldata=="--Select--")
                celldata="";//wierd anomaly in "Managing Team"
              ro[colmns[j]]=celldata;
            }
            data.push(ro);
          }
        }
        if (hgrid){
          hgrid.itemsSource= data;
        } else {
          $("<div id='hiddenGrid' style='display:none'></div>").appendTo('body');
          hgrid = new wijmo.grid.FlexGrid('#hiddenGrid', {
            itemsSource: data
          });
        }
        exportgrid=hgrid;
      }
      var exl_options={
        includeColumnHeaders: true,
        includeRowHeaders: false
      };
      var book = wijmo.grid.xlsx.FlexGridXlsxConverter.save(exportgrid, exl_options, staticviews.title+".xlsx");
    }//exportToExcel
    var hgrid=null;

    staticviews.refresh_view = function(){
      if(!$.isEmptyObject(staticviews.grid))
      {
        staticviews.grid.dispose();
        staticviews.grid = {};
      }
      staticviews.clearSubgridFilters();
    }

    staticviews.startRowLoading = function(s, e){
      $("#waitLogo").show();
    }

    staticviews.endRowLoading = function(s, e){
      $("#waitLogo").hide();
    }

    staticviews.handleSubgridButtons = function(applyCallback){
      staticviews.clearSubgridButtons();
      $("#be_staticviews_expandfilters").click(function(){
        $("#be_staticviews_filters").toggle();
        if($("#be_staticviews_filters").is(":visible")){
          $("#be_staticviews_expandfilters span").addClass("wj-glyph-up");
          $("#be_staticviews_expandfilters span").removeClass("wj-glyph-down");
        } else {
          $("#be_staticviews_expandfilters span").addClass("wj-glyph-down");
          $("#be_staticviews_expandfilters span").removeClass("wj-glyph-up");
        }
      });
      $("#be_staticviews_btn_applyfilters").click(applyCallback);
      $("#be_staticviews_btn_clear").click(staticviews.clearSubgridFilters);

    }

    staticviews.clearSubgridButtons = function(){
      $("#be_staticviews_filters").hide();
      $("#be_staticviews_expandfilters").off("click");
      $("#be_staticviews_btn_applyfilters").off("click");
      //$("#be_staticviews_btn_clear").off("click");
    }

    staticviews.clearSubgridFilters = function(){
      staticviews.showFilteredResults = false;
      staticviews.filterids = [];
      staticviews.selectedItems = [];
      if(!$.isEmptyObject(staticviews.grid)){
        staticviews.grid.itemsSource.refresh();
      }
    }

    staticviews.handleSelectionChange = function() {
      // return;//numan
      var height=staticviews.grid.cells.rows.length;
      for (var i=0;i<height;i++){
        staticviews.grid.rows[i].isSelected = false;
      }
      staticviews.selectedItems.forEach(function(row){
        row.isSelected = true;
      });
    }

    staticviews.headerFormatter = function(panel, r, c, cell)
    {
      var col = panel.columns[c];
      if(panel.cellType == wijmo.grid.CellType.TopLeft){
        switch(col.name)
        {
          case "Is Selected":
          var selCount = staticviews.selectedItems.length;
          var rowCount = staticviews.grid.rows.length;

          cell.innerHTML = "<input type='checkbox'>";
          var cb = cell.firstChild;
          cb.checked = (selCount > 0);
          cb.indeterminate = (selCount>0) && (selCount!=rowCount);

          cb.addEventListener('click', function (ev) {
            staticviews.grid.beginUpdate();
            staticviews.selectedItems = [];
            if(this.checked)
            {
              staticviews.grid.rows.forEach(function(row){
                staticviews.selectedItems.push(row);
                row.isSelected = true;
              });
            }
            staticviews.grid.endUpdate();
            staticviews.grid.refresh();
            ev.stopPropagation();
          });

          break;
        }
      }
      else if(panel.cellType == wijmo.grid.CellType.RowHeader){
        switch(col.name)
        {
          case "Is Selected":
          var checked = (staticviews.selectedItems.indexOf(staticviews.grid.rows[r])>-1);
          var selected = staticviews.grid.rows[r].isSelected;
          cell.innerHTML = "<input type='checkbox'>";
          /*
          cell.className = "wj-cell ";
          cell.className += checked||selected ? "wj-state-multi-selected" : "wj-alt";
          */
          var cb = cell.firstChild;
          cb.checked = checked;

          staticviews.grid.rows[r].isSelected = checked;

          cb.addEventListener('click', function (ev) {
            staticviews.grid.beginUpdate();
            if(this.checked)
            {
              staticviews.selectedItems.push(staticviews.grid.rows[r]);
              staticviews.grid.rows[r].isSelected = true;
            }
            else {
              var index = staticviews.selectedItems.indexOf(staticviews.grid.rows[r]);
              if(index > -1){
                staticviews.selectedItems.splice(index, 1);
              }
              staticviews.grid.rows[r].isSelected = false;
            }
            staticviews.grid.endUpdate();
            //staticviews.grid.refresh();
            ev.stopPropagation();
          });

          break;
        }
      }
    }



    function itemFormatter(panel, r, c, cell){
      if(panel.rows[r] instanceof wijmo.grid.detail.DetailRow)
      return;

      if(panel.cellType == wijmo.grid.CellType.TopLeft || panel.cellType == wijmo.grid.CellType.RowHeader)
      {
        staticviews.headerFormatter(panel, r, c, cell);
      }
      else if (panel.cellType == wijmo.grid.CellType.Cell && !(panel.rows[r] instanceof wijmo.grid.detail.DetailRow))
      {

        // use chartInfo to draw a bar chart
        var col = panel.columns[c];

        // create other types of custom content
        switch (col.name)
        {
          case 'Name':
          cell.innerHTML =
          "<a href='../mps_/processdashboardPopup.html?data=" + panel.rows[r].dataItem['mps_applicationid'] + "' target='_blank'>" + panel.rows[r].dataItem['mps_name'] + "</a>";
          break;
          case 'Contact':
          var cid = panel.rows[r].dataItem["_mps_contact_value"];
          var cname = panel.grid.columns[c].dataMap.getDisplayValue(cid);
          cell.innerHTML = "<a href='../mps_/clientDetails.html?data=" + cid + "' target='_blank'>" + cname + "</a>";
          /*cell.innerHTML += "<div role='button' class='wj-elem-dropdown'><span class='wj-glyph-down'></span></div>";*/
          break;
          case 'Lender':
          if(panel.rows[r].dataItem["_mps_lender_value"])
          {
            var lid = panel.rows[r].dataItem["_mps_lender_value"];
            var lobj = panel.grid.columns[c].dataMap.getDisplayValue(lid);
            if(lobj.imgpath)
            {
              cell.innerHTML = "<span style='display:none;'>"+lobj.name+"</span><img src='" + lobj.imgpath + "' alt='" + lobj.name + "' height='32' width='32'>";
              cell.innerHTML += "<div role='button' class='wj-elem-dropdown'><span class='wj-glyph-down'></span></div>";
            }
          }
          break;
        }
      }
    }

    function assigncontactsmap()
    {
      var url = brokerengine.odataurl;
      var keymap = [];
      $.getJSON(url + "/contacts?$select=fullname", function(data){
        var col = staticviews.grid.columns.getColumn("Contact");
        col.dataMap = new wijmo.grid.DataMap(data.value, "contactid", "fullname");
      });
    }

    function assignlendersmap()
    {
      var url = brokerengine.odataurl;
      var keymap = [];
      $.getJSON(url + "/mps_lenderses?$select=mps_lenders1,entityimage_url", function(data){
        data.value.forEach(function(lender){
          keymap.push({mps_lendersid: lender.mps_lendersid, lender: new brokerengine.entities.Lender(lender.mps_lendersid, lender.mps_lenders1, lender.entityimage_url)});
        });
        var col = staticviews.grid.columns.getColumn("Lender");
        col.dataMap = new wijmo.grid.DataMap(keymap, "mps_lendersid", "lender");
      });
    }

    function assignappstagemap()
    {
      var url = brokerengine.odataurl;
      var keymap = [];
      $.getJSON(url + "/mps_stages?$select=mps_stage1&$filter=mps_entitytype%20eq%20922600000%20and%20statecode%20eq%200", function(data){
        var col = staticviews.grid.columns.getColumn("Application Stage");
        col.dataMap = new wijmo.grid.DataMap(data.value, "mps_stageid", "mps_stage1");
      });
    }

    function assignownersmap()
    {
      var url = brokerengine.odataurl;
      var keymap = [];
      $.getJSON(url + "/systemusers?$select=fullname", function(data){
        var col = staticviews.grid.columns.getColumn("Owner");
        col.dataMap = new wijmo.grid.DataMap(data.value, "systemuserid", "fullname");
      });
    }

  })(brokerengine.staticviews = brokerengine.staticviews || {});
})(window.brokerengine = window.brokerengine || {}, jQuery);
