(function(brokerengine, $, undefined){
  (function(entities){
    function Contact(id, name){
      this.contactid = id;
      this.name = name;
      this.validName = true;
      if(name == id){
        this.validName = false;
        this.name = " (unknown) ";
      }

      this.getItemUrl = function()
      {
        var url = "";
        if(this.validName){
          url = "<a href='../mps_/clientDetails.html?data=" + this.contactid + "' target='_blank'>" + this.name + "</a>";
        }
        else{
          url = this.name;
        }
        return url;
      }
    }
    entities.Contact = Contact;

    (function(contacts){
      var entityName = "contacts";
      var entityUrl = brokerengine.odataurl + "/" + entityName;
      var activefilter = "$filter=statecode eq 0";

      contacts.dataMap = {};

      contacts.contacttypes = [
        { Label:"Client", Value: 922600000 },
        { Label:"Referrer", Value: 922600001 },
        { Label:"Real Estate Agent", Value: 922600002 },
        { Label:"Solicitor", Value: 922600003 },
        { Label:"Accountant", Value: 922600004 },
        { Label:"Financial Planner", Value: 922600005 },
        { Label:"Other", Value: 922600006 },
        { Label:"BDM", Value: 922600007 },
        { Label:"Builder", Value: 922600008 },
        { Label:"Contact Type", Value: 864880000 },
      ];

      contacts.fetchById = function(cid, callback, fields)
      {
        var url = entityUrl + "(" + cid + ")";
        if(fields)
        {
          url += "?$select=" + fields;
        }

        $.ajax({
          url: url,
          dataType: "json"
        })
          .done(function(contact){
            callback(contact);
          });
      }

      contacts.assignColumnMap = function(grid, colname)
      {
        var col = grid.columns.getColumn(colname);
        switch(colname){
          case "Contact Type":
            col.dataMap = new wijmo.grid.DataMap(contacts.contacttypes, "Value", "Label");
          break;
          case "Review Frequency":
            col.dataMap = new wijmo.grid.DataMap(brokerengine.reviewfrequency, "Value", "Label");
          break;
        }
      }

      contacts.assignContactsMap = function(grid, colname)
      {
        var col = grid.columns.getColumn(colname);
        if($.isEmptyObject(contacts.dataMap))
        {
          var url = entityUrl + "?$select=fullname";
          brokerengine.getData(url, function(contacts){
            brokerengine.entities.contacts.dataMap = new wijmo.grid.DataMap(contacts, "contactid", "fullname");
            col.dataMap = brokerengine.entities.contacts.dataMap;            
          });
        /*
          $.ajax({
            async: false,
            dataType: "json",
            url: url,
            success: function(data)
            {
              contacts.dataMap = new wijmo.grid.DataMap(data.value, "contactid", "fullname");
            }
          });
        */
        }
        col.dataMap = contacts.dataMap;
      }
    })(entities.contacts = entities.contacts || {});

  })(brokerengine.entities = brokerengine.entities || {});

})(window.brokerengine = window.brokerengine || {}, jQuery);
