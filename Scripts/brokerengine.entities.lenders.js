(function(brokerengine, $, undefined)
{
  (function(entities)
  {
    function Lender(id, name, imgpath) {
      this.lenderid = id;
      this.name = name;
      this.imgpath = imgpath;

      Lender.prototype.toString = function() {
        return this.name;
      }

      Lender.prototype.toLowerCase = function() {
        return this.name.toLowerCase();
      }
    }
    entities.Lender = Lender;

    (function(lenders){
      var entityName = "mps_lenderses";
      var entityUrl = brokerengine.odataurl + "/" + entityName;
      lenders.dataMap = {};

      lenders.assignColumnMap = function(grid, col)
      {
        var col = grid.columns.getColumn(col);
        if($.isEmptyObject(lenders.dataMap))
        {
          var keymap = lenders.fetchLenders();
          lenders.dataMap = new wijmo.grid.DataMap(keymap, "mps_lendersid", "lender");
        }
        col.dataMap = lenders.dataMap;
      }

      lenders.fetchLenders = function()
      {
        var keymap = [];
        var url = entityUrl + "?$select=mps_lenders1,entityimage_url";
        if(!$.isEmptyObject(lenders.dataMap))
        {
          keymap = lenders.dataMap.collectionView.items;
        }
        else {
          $.ajax({
            async: false,
            dataType: "json",
            url: url,
            success: function(data)
            {
              data.value.forEach(function(lender){
                keymap.push({mps_lendersid: lender.mps_lendersid, lender: new brokerengine.entities.Lender(lender.mps_lendersid, lender.mps_lenders1, lender.entityimage_url)});
              });
            }
          });
        }
        return keymap;
      }

      lenders.formatCell = function(dataitem, colname)
      {
        var html;
        if(dataitem[colname] && !$.isEmptyObject(lenders.dataMap))
        {
          var lid = dataitem[colname];
          var lobj = lenders.dataMap.getDisplayValue(lid);
          if(lobj && lobj.imgpath) {
            html = "<span style='display: inline-block;width: 100%;text-align: center;'>"+
            "<span style='display:none;'>"+lobj.name+"</span>"+
            "<img src='" + lobj.imgpath + "' alt='" + lobj.name + "' height='32' width='32' /></span>";
            // html += "<div role='button' class='wj-elem-dropdown'><span class='wj-glyph-down'></span></div>";
          }
        }
        return html;
      }
    })(entities.lenders = entities.lenders || {});

  })(brokerengine.entities = brokerengine.entities || {});

})(window.brokerengine = window.brokerengine || {}, jQuery);
