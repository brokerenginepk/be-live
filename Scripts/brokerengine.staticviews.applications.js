(function(brokerengine, $, undefined){

  (function(staticviews)  {

    (function(applications){

      var shareHandler = {};

      applications.initializeView = function(){
        applications_views = [
          "",
          "Applications/Splits",
          "Applications/Tasks"
        ];
        var selectView = {};
        selectView = new wijmo.input.ComboBox("#be_staticviews_selectview");
        selectView.itemsSource = applications_views;
        selectView.selectedIndex=1;
        selectView.textChanged.addHandler(changeView);
        applications.selectView = selectView;

        staticviews.title = "Applications/Splits";
        $("#be_staticviews_title").html("<H1> " + staticviews.title + " </H1>");
        brokerengine.staticviews.app_fundingpos.initialize("#be_view");

        staticviews.handleExportButtons();
        staticviews.handleDeactivateButton(deactivateRecords);

        shareHandler = new brokerengine.share.ShareApps();
				shareHandler.initialize(true);
        $("#be_deals_share").click(shareDeals);
      }

      function changeView()
      {
        var newview = applications.selectView.selectedValue;
        switch(newview){
          case "Applications/Splits":
          brokerengine.staticviews.title = "Applications/Splits";
          brokerengine.staticviews.app_fundingpos.initialize("#be_view");
          break;
          case "Applications/Tasks":
          brokerengine.staticviews.title = "Applications/Tasks";
          brokerengine.staticviews.app_tasks.initialize("#be_view");
          break;
        }
        $("#be_staticviews_title").html("<H1> " + staticviews.title + " </H1>");
    }

      function deactivateRecord(ent, id) {
        var entity = {};
        entity.statuscode = 2;
        entity.statecode = 1;
        var req = new XMLHttpRequest();
        req.open("PATCH", brokerengine.odataurl+"/"+
        ent+"("+id+")", true);
        req.setRequestHeader("OData-MaxVersion", "4.0");
        req.setRequestHeader("OData-Version", "4.0");
        req.setRequestHeader("Accept", "application/json");
        req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
        req.onreadystatechange = function () {
          if (this.readyState === 4) {
            req.onreadystatechange = null;
            if (this.status === 204) {
            }
            else {
              Xrm.Utility.alertDialog(this.statusText);
            }
          }
        };
        req.send(JSON.stringify(entity));
      }

      deactivateRecords=function() {
        if (confirm("Are you sure that you want to delete the selected records?")) {
          var height=staticviews.grid.cells.rows.length;
          for (var i=0;i<height;i++){
            if(staticviews.grid.rows[i].isSelected){
              deactivateRecord("mps_applications",staticviews.grid.rows[i].dataItem.mps_applicationid);
            }
          }
          // Refresh the view
          setTimeout(function(){ changeView();}, 3000);
        }
      }

      function shareDeals()
      {
        var deals = [];
        staticviews.selectedItems.forEach(function(approw){
          deals.push(approw.dataItem);
        });
        shareHandler.openDialog(deals);


        /*
        var selcount = staticviews.selectedItems.length;
        var dealids = [];
        var relatedrecords = {};
        var contacts = [];
        var brokers = [];
        var lenderdetails = [];
        var brands = [];
        var parentid;
        var ownerid;
        var lenderdetailsid;

        staticviews.selectedItems.forEach(function(approw){
          dealids.push(approw.dataItem.mps_applicationid);
          parentid = approw.dataItem._mps_contact_value;
          if(parentid && !contacts.includes(parentid))
          {
            contacts.push(parentid);
          }
          ownerid = approw.dataItem._ownerid_value;
          if(ownerid && !brokers.includes(ownerid))
          {
            brokers.push(ownerid);
          }
          lenderdetailsid = approw.dataItem._mps_brokerlenderdetails_value;
          if(lenderdetailsid && !lenderdetails.includes(lenderdetailsid))
          {
            lenderdetails.push(lenderdetailsid);
          }
        });

        relatedrecords.contacts = contacts;
        relatedrecords.brokers = brokers;
        relatedrecords.lenderdetails = lenderdetails;

        brokerengine.share.ShareDeals(dealids, relatedrecords);
        */
      }

    })(staticviews.applications = staticviews.applications || {});

    (function(app_fundingpos){
      var loanamount_op = {};
      var loanamount_val = {};
      var producttype_op = {};
      var producttype_val = {};
      var loanpurpose_op = {};
      var loanpurpose_val = {};
      var interestrate_op = {};
      var interestrate_val = {};
      var pricingdiscount_op = {};
      var pricingdiscount_val = {};
      var netrate_op = {};
      var netrate_val = {};
      var paymentoptions_op = {};
      var paymentoptions_val = {};
      var fixedrateexpritydate_op = {};
      var fixedrateexpritydate_val = {};
      var interestonlyexpirydate_op = {};
      var interestonlyexpirydate_val = {};
      var loanbsb_op = {};
      var loanbsb_val = {};
      var loanaccnum_op = {};
      var loanaccnum_val = {};

      app_fundingpos.initialize = function(id){
        brokerengine.staticviews.startRowLoading();

        // initialize view filter controls
        configureSubgridFilters();

        brokerengine.staticviews.refresh_view();

        // Should call brokerengine.entities.applications.generateGrid wit odata=true
        staticviews.grid = brokerengine.entities.applications.generateGrid(id, null, true, subgridFilter);
        staticviews.grid.columns.moveElement(2, 4);

        var filter = new wijmo.grid.filter.FlexGridFilter(staticviews.grid);
        var cf = filter.getColumnFilter("Status").valueFilter;
        cf.showValues = {
          'Active': true,
        };
        filter.apply();
        brokerengine.staticviews.filter = filter;
        staticviews.grid.rowHeaders.columns.push(new wijmo.grid.Column(staticviews.rowSelectionColumn));
        staticviews.grid.selectionChanged.addHandler(staticviews.handleSelectionChange);

        var detailProvider = new wijmo.grid.detail.FlexGridDetailProvider(staticviews.grid);
        detailProvider.createDetailCell = function(row)
        {
          return configureDetailsGrid(row.dataItem);
        }

        brokerengine.staticviews.endRowLoading();
      };
      /*ns*/
      function get_today(){
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth()+1; //January is 0!
        var yyyy = today.getFullYear();
        if(dd<10) {dd = '0'+dd}
        if(mm<10) {mm = '0'+mm}
        today = dd + '-' + mm + '-' + yyyy;
        return today;
      }
      function clear_filter(){
        loanamount_op.selectedIndex=0;
        producttype_op.selectedIndex=0;
        loanpurpose_op.selectedIndex=0;
        interestrate_op.selectedIndex=0;
        pricingdiscount_op.selectedIndex=0;
        netrate_op.selectedIndex=0;
        paymentoptions_op.selectedIndex=0;
        fixedrateexpritydate_op.selectedIndex=0;
        interestonlyexpirydate_op.selectedIndex=0;
        loanbsb_op.selectedIndex=0;
        loanaccnum_op.selectedIndex=0;

        $("#be_appview_val_loanamount input").val("$0");
        $("#be_appview_val_producttype input").val("--Select--");
        $("#be_appview_val_loanpurpose input").val("--Select--");
        $("#be_appview_val_interestrate input").val("0");
        $("#be_appview_val_pricingdiscount input").val("0");
        $("#be_appview_val_netrate input").val("0");
        $("#be_appview_val_paymentoptions input").val("");
        $("#be_appview_val_fixedrateexpirydate input").val(get_today());
        $("#be_appview_val_interestonlyexpirydate input").val(get_today());
        $("#be_appview_val_loanbsb input").val("");
        $("#be_appview_val_loanaccnum input").val("");
      }

      $("#be_staticviews_btn_clear").click(clear_filter);

      function configureDetailsGrid(appRow){
        staticviews.startRowLoading();
        var view = {};
        var griddetails = {};
        var cell = document.createElement('div');

        var colsarr = [
          { binding: "LoanAmount", name: "Loan Amount", header: "Loan Amount", format: "c0", width: "5*", minWidth: 125 },
          { binding: "ProductType", name: "Product Type", header: "Product Type", width: "5*", minWidth: 125 },
          { binding: "LoanPurpose", name: "Loan Purpose", header: "Loan Purpose", width: "5*", minWidth: 125 },
          { binding: "InterestRate", name: "Interest Rate", header: "Interest Rate", width: "5*", minWidth: 125 },
          { binding: "PricingDiscount", name: "Pricing Discount", header: "Pricing Discount", width: "5*", minWidth: 125 },
          { binding: "NetRate", name: "Net Rate", header: "Net Rate", width: "5*", minWidth: 125 },
          { binding: "PMTOptions", name: "PMT Options", header: "PMT Options", width: "5*", minWidth: 125 },
          { binding: "FixedRateExpiryDate", name: "Fixed Rate Expiry Date", header: "Fixed Rate Expiry Date", width: "5*", minWidth: 125, dataType: wijmo.DataType.Date, format: "dd-MM-yyyy" },
          { binding: "InterestOnlyExpiryDate", name: "Interest Only Expiry Date", header: "Interest Only Expiry Date", width: "5*", minWidth: 125, dataType: wijmo.DataType.Date, format: "dd-MM-yyyy" },
          { binding: "LoanBSB", name: "Loan BSB", header: "Loan BSB", minWidth: 125 },
          { binding: "LoanAccNumber", name: "Loan Account Number", header: "Loan Account Number", width: "5*", width: "5*", minWidth: 125 }
        ];

        var splits_arr = [];
        brokerengine.entities.fundingpositions.fetchDefaultSplits(appRow, function(splits){
          splits.forEach(function(split){
            splits_arr.push(split);
          });
          view.refresh();
          griddetails.refresh();
          brokerengine.staticviews.grid.autoSizeRows();
        });
        view = new wijmo.collections.CollectionView(splits_arr);
        griddetails = new wijmo.grid.FlexGrid(cell, {
          columns: colsarr,
          autoGenerateColumns: false,
          itemsSource: view,
          loadingRows: staticviews.startRowLoading,
          loadedRows: staticviews.endRowLoading,
          showAlternatingRows: true
        });
        var filter = new wijmo.grid.filter.FlexGridFilter(griddetails);

        var col = {};
        col = griddetails.columns.getColumn("Product Type");
        col.dataMap = new wijmo.grid.DataMap(brokerengine.producttype, "Value", "Label");
        col = griddetails.columns.getColumn("Loan Purpose");
        col.dataMap = new wijmo.grid.DataMap(brokerengine.purpose, "Value", "Label");
        col = griddetails.columns.getColumn("PMT Options");
        col.dataMap = new wijmo.grid.DataMap(brokerengine.pmtoptions, "Value", "Label");

        staticviews.endRowLoading();
        return cell;
      }

      function getFundingPositions(application){
        var fpn = application.mps_FundingPositionNew;
        var splitsarr = [];
        var split = {};
        if (typeof fpn !== 'undefined' && fpn!=null) {
          if(fpn.mps_loanamount1){
            split = {
              LoanAmount: fpn.mps_loanamount1,
              ProductType: fpn.mps_producttypeloantype1,
              LoanPurpose: fpn.mps_loanpurposesplit1,
              InterestRate: fpn.mps_interestrate1,
              PricingDiscount: fpn.mps_pricingdiscount1,
              NetRate: fpn.mps_netrate1,
              PMTOptions: fpn.mps_pmtoption1,
              FixedRateExpiryDate: fpn.mps_fixedrateexpirydate1?new Date(fpn.mps_fixedrateexpirydate1):null,
              InterestOnlyExpiryDate: fpn.mps_interestonlyexpirydate1?new Date(fpn.mps_interestonlyexpirydate1):null,
              LoanBSB: fpn.mps_loanbsb1,
              LoanAccNumber: fpn.mps_loanaccountnumber1
            }
            splitsarr.push(split);
          }
          if(fpn.mps_loanamount2){
            split = {
              LoanAmount: fpn.mps_loanamount2,
              ProductType: fpn.mps_producttypeloantype2,
              LoanPurpose: fpn.mps_loanpurposesplit2,
              InterestRate: fpn.mps_interestrate2,
              PricingDiscount: fpn.mps_pricingdiscount2,
              NetRate: fpn.mps_netrate2,
              PMTOptions: fpn.mps_pmtoption2,
              FixedRateExpiryDate: fpn.mps_fixedrateexpirydate2?new Date(fpn.mps_fixedrateexpirydate2):null,
              InterestOnlyExpiryDate: fpn.mps_interestonlyexpirydate2?new Date(fpn.mps_interestonlyexpirydate2) : null,
              LoanBSB: fpn.mps_loanbsb2,
              LoanAccNumber: fpn.mps_loanaccountnumber2
            }
            splitsarr.push(split);
          }
          if(fpn.mps_loanamount3){
            split = {
              LoanAmount: fpn.mps_loanamount3,
              ProductType: fpn.mps_producttypeloantype3,
              LoanPurpose: fpn.mps_loanpurposesplit3,
              InterestRate: fpn.mps_interestrate3,
              PricingDiscount: fpn.mps_pricingdiscount3,
              NetRate: fpn.mps_netrate3,
              PMTOptions: fpn.mps_pmtoption3,
              FixedRateExpiryDate: fpn.mps_fixedrateexpirydate3?new Date(fpn.mps_fixedrateexpirydate3):null,
              InterestOnlyExpiryDate: fpn.mps_interestonlyexpirydate3?new Date(fpn.mps_interestonlyexpirydate3) : null,
              LoanBSB: fpn.mps_loanbsb3,
              LoanAccNumber: fpn.mps_loanaccountnumber3
            }
            splitsarr.push(split);
          }
          if(fpn.mps_loanamount4){
            split = {
              LoanAmount: fpn.mps_loanamount4,
              ProductType: fpn.mps_producttypeloantype4,
              LoanPurpose: fpn.mps_loanpurposesplit4,
              InterestRate: fpn.mps_interestrate4,
              PricingDiscount: fpn.mps_pricingdiscount4,
              NetRate: fpn.mps_netrate4,
              PMTOptions: fpn.mps_pmtoption4,
              FixedRateExpiryDate: fpn.mps_fixedrateexpirydate4?new Date(fpn.mps_fixedrateexpirydate4):null,
              InterestOnlyExpiryDate: fpn.mps_interestonlyexpirydate4?new Date(fpn.mps_interestonlyexpirydate1) : null,
              LoanBSB: fpn.mps_loanbsb4,
              LoanAccNumber: fpn.mps_loanaccountnumber4
            }
            splitsarr.push(split);
          }
          if(fpn.mps_loanamount5){
            split = {
              LoanAmount: fpn.mps_loanamount5,
              ProductType: fpn.mps_producttypeloantype5,
              LoanPurpose: fpn.mps_loanpurposesplit5,
              InterestRate: fpn.mps_interestrate5,
              PricingDiscount: fpn.mps_pricingdiscount5,
              NetRate: fpn.mps_netrate5,
              PMTOptions: fpn.mps_pmtoption5,
              FixedRateExpiryDate: fpn.mps_fixedrateexpirydate5 ? new Date(fpn.mps_fixedrateexpirydate5) : null ,
              InterestOnlyExpiryDate: fpn.mps_interestonlyexpirydate5?new Date(fpn.mps_interestonlyexpirydate5) : null,
              LoanBSB: fpn.mps_loanbsb5,
              LoanAccNumber: fpn.mps_loanaccountnumber5
            }
            splitsarr.push(split);
          }
        }
        return splitsarr;
      }

      function subgridFilter(app){
//        var display = staticviews.filter._filter(app) && app.statecode==0;
        var display = staticviews.filter._filter(app);
        if(staticviews.showFilteredResults)
        {
          return (staticviews.filterids.indexOf(app.mps_applicationid) > -1) && display;
        }
        return display;
      }

      function configureSubgridFilters(){
        // initialize buttons. Provide callback function for Apply filters button
        staticviews.handleSubgridButtons(applySubgridFilters);

        if($.isEmptyObject(loanamount_op))
        {
          // Loan Amount
          loanamount_op = brokerengine.createFilterOperator("#be_appview_op_loanamount", wijmo.DataType.Number);
          loanamount_val = brokerengine.createFilterInput("#be_appview_val_loanamount", wijmo.DataType.Number, "c0");
          // Product Type
          producttype_op = brokerengine.createFilterOperator("#be_appview_op_producttype", wijmo.DataType.String);
          producttype_val = brokerengine.createDatamapInput("#be_appview_val_producttype", brokerengine.producttype, "Value", "Label");
          // LoanPurpose
          loanpurpose_op = brokerengine.createFilterOperator("#be_appview_op_loanpurpose", wijmo.DataType.String);
          loanpurpose_val = brokerengine.createDatamapInput("#be_appview_val_loanpurpose", brokerengine.purpose, "Value", "Label");
          // InterestRate
          interestrate_op = brokerengine.createFilterOperator("#be_appview_op_interestrate", wijmo.DataType.Number);
          interestrate_val = brokerengine.createFilterInput("#be_appview_val_interestrate", wijmo.DataType.Number);
          // PricingDiscount
          pricingdiscount_op = brokerengine.createFilterOperator("#be_appview_op_pricingdiscount", wijmo.DataType.Number);
          pricingdiscount_val = brokerengine.createFilterInput("#be_appview_val_pricingdiscount", wijmo.DataType.Number);
          // NetRate
          netrate_op = brokerengine.createFilterOperator("#be_appview_op_netrate", wijmo.DataType.Number);
          netrate_val = brokerengine.createFilterInput("#be_appview_val_netrate", wijmo.DataType.Number);
          // PMTOptions
          paymentoptions_op = brokerengine.createFilterOperator("#be_appview_op_paymentoptions", wijmo.DataType.String);
          paymentoptions_val = brokerengine.createFilterInput("#be_appview_val_paymentoptions", brokerengine.pmtoptions, "Value", "Label");
          // FixedRateExpiryDate
          fixedrateexpritydate_op = brokerengine.createFilterOperator("#be_appview_op_fixedrateexpritydate", wijmo.DataType.Date);
          fixedrateexpritydate_val = brokerengine.createFilterInput("#be_appview_val_fixedrateexpirydate", wijmo.DataType.Date, "dd-MM-yyyy");
          // InterestOnlyExpiryDate
          interestonlyexpirydate_op = brokerengine.createFilterOperator("#be_appview_op_interestonlyexpirydate", wijmo.DataType.Date);
          interestonlyexpirydate_val = brokerengine.createFilterInput("#be_appview_val_interestonlyexpirydate", wijmo.DataType.Date, "dd-MM-yyyy");
          // LoanBSB
          loanbsb_op = brokerengine.createFilterOperator("#be_appview_op_loanbsb", wijmo.DataType.String);
          loanbsb_val = brokerengine.createFilterInput("#be_appview_val_loanbsb", wijmo.DataType.String);
          // LoanAccNumber
          loanaccnum_op = brokerengine.createFilterOperator("#be_appview_op_loanaccnum", wijmo.DataType.String);
          loanaccnum_val = brokerengine.createFilterInput("#be_appview_val_loanaccnum", wijmo.DataType.String);
        }
      }

      function applySubgridFilters()
      {
        var filter = getFilterString();
        if(filter)
        {
          staticviews.showFilteredResults = true;
          staticviews.filterids = fetchFilteredApps(filter);
          staticviews.selectedItems = [];
          staticviews.grid.itemsSource.refresh();
        }
      }

      function getFilterString(){
        var filter = ""
        var op;
        var value;
        if(loanamount_op.selectedIndex > 0)
        {
          op = loanamount_op.selectedValue;
          field = "mps_loanamount";
          value = loanamount_val.value;

          filter += "(";
          filter += brokerengine.getFilterString(op, field+"1", value);
          filter += " or " + brokerengine.getFilterString(op, field+"2", value);
          filter += " or " + brokerengine.getFilterString(op, field+"3", value);
          filter += " or " + brokerengine.getFilterString(op, field+"4", value);
          filter += " or " + brokerengine.getFilterString(op, field+"5", value);
          filter += ")";
        }
        if(producttype_op.selectedIndex > 0)
        {
          if(filter)
          {
            filter += " and ";
          }
          op = producttype_op.selectedValue;
          field = "mps_producttypeloantype";
          value = producttype_val.selectedValue;

          filter += "(";
          filter += brokerengine.getFilterString(op, field+"1", value);
          filter += " or " + brokerengine.getFilterString(op, field+"2", value);
          filter += " or " + brokerengine.getFilterString(op, field+"3", value);
          filter += " or " + brokerengine.getFilterString(op, field+"4", value);
          filter += " or " + brokerengine.getFilterString(op, field+"5", value);
          filter += ")";
        }
        if(loanpurpose_op.selectedIndex > 0)
        {
          if(filter)
          {
            filter += " and ";
          }
          op = loanpurpose_op.selectedValue;
          field = "mps_loanpurposesplit";
          value = loanpurpose_val.selectedValue;

          filter += "(";
          filter += brokerengine.getFilterString(op, field+"1", value);
          filter += " or " + brokerengine.getFilterString(op, field+"2", value);
          filter += " or " + brokerengine.getFilterString(op, field+"3", value);
          filter += " or " + brokerengine.getFilterString(op, field+"4", value);
          filter += " or " + brokerengine.getFilterString(op, field+"5", value);
          filter += ")";
        }
        if(interestrate_op.selectedIndex > 0)
        {
          if(filter)
          {
            filter += " and ";
          }
          op = interestrate_op.selectedValue;
          field = "mps_interestrate";
          value = interestrate_val.value;

          filter += "(";
          filter += brokerengine.getFilterString(op, field+"1", value);
          filter += " or " + brokerengine.getFilterString(op, field+"2", value);
          filter += " or " + brokerengine.getFilterString(op, field+"3", value);
          filter += " or " + brokerengine.getFilterString(op, field+"4", value);
          filter += " or " + brokerengine.getFilterString(op, field+"5", value);
          filter += ")";
        }
        if(pricingdiscount_op.selectedIndex > 0)
        {
          if(filter)
          {
            filter += " and ";
          }
          op = pricingdiscount_op.selectedValue;
          field = "mps_pricingdiscount";
          value = pricingdiscount_val.value;

          filter += "(";
          filter += brokerengine.getFilterString(op, field+"1", value);
          filter += " or " + brokerengine.getFilterString(op, field+"2", value);
          filter += " or " + brokerengine.getFilterString(op, field+"3", value);
          filter += " or " + brokerengine.getFilterString(op, field+"4", value);
          filter += " or " + brokerengine.getFilterString(op, field+"5", value);
          filter += ")";
        }
        if(netrate_op.selectedIndex > 0)
        {
          if(filter)
          {
            filter += " and ";
          }
          op = netrate_op.selectedValue;
          field = "mps_netrate";
          value = netrate_val.value;

          filter += "(";
          filter += brokerengine.getFilterString(op, field+"1", value);
          filter += " or " + brokerengine.getFilterString(op, field+"2", value);
          filter += " or " + brokerengine.getFilterString(op, field+"3", value);
          filter += " or " + brokerengine.getFilterString(op, field+"4", value);
          filter += " or " + brokerengine.getFilterString(op, field+"5", value);
          filter += ")";
        }
        if(paymentoptions_op.selectedIndex > 0)
        {
          if(filter)
          {
            filter += " and ";
          }
          op = paymentoptions_op.selectedValue;
          field = "mps_pmtoption";
          value = paymentoptions_val.value;

          filter += "(";
          filter += brokerengine.getFilterString(op, field+"1", value);
          filter += " or " + brokerengine.getFilterString(op, field+"2", value);
          filter += " or " + brokerengine.getFilterString(op, field+"3", value);
          filter += " or " + brokerengine.getFilterString(op, field+"4", value);
          filter += " or " + brokerengine.getFilterString(op, field+"5", value);
          filter += ")";
        }
        if(fixedrateexpritydate_op.selectedIndex > 0)
        {
          // TO BE FIXED FOR DATE FILTER
          if(filter)
          {
            filter += " and ";
          }
          op = fixedrateexpritydate_op.selectedValue;
          field = "mps_fixedrateexpirydate";
          value = fixedrateexpritydate_val.value;

          filter += "(";
          filter += brokerengine.getFilterDate(op, field+"1", value);
          filter += " or " + brokerengine.getFilterDate(op, field+"2", value);
          filter += " or " + brokerengine.getFilterDate(op, field+"3", value);
          filter += " or " + brokerengine.getFilterDate(op, field+"4", value);
          filter += " or " + brokerengine.getFilterDate(op, field+"5", value);
          filter += ")";
        }
        if(interestonlyexpirydate_op.selectedIndex > 0)
        {
          // TO BE FIXED FOR DATE FILTER
          if(filter)
          {
            filter += " and ";
          }
          op = interestonlyexpirydate_op.selectedValue;
          field = "mps_interestonlyexpirydate";
          value = interestonlyexpirydate_val.value;

          filter += "(";
          filter += brokerengine.getFilterDate(op, field+"1", value);
          filter += " or " + brokerengine.getFilterDate(op, field+"2", value);
          filter += " or " + brokerengine.getFilterDate(op, field+"3", value);
          filter += " or " + brokerengine.getFilterDate(op, field+"4", value);
          filter += " or " + brokerengine.getFilterDate(op, field+"5", value);
          filter += ")";
        }
        if(loanbsb_op.selectedIndex > 0)
        {
          if(filter)
          {
            filter += " and ";
          }
          op = loanbsb_op.selectedValue;
          field = "mps_loanbsb";
          value = loanbsb_val.value;

          filter += "(";
          filter += brokerengine.getFilterString(op, field+"1", value);
          filter += " or " + brokerengine.getFilterString(op, field+"2", value);
          filter += " or " + brokerengine.getFilterString(op, field+"3", value);
          filter += " or " + brokerengine.getFilterString(op, field+"4", value);
          filter += " or " + brokerengine.getFilterString(op, field+"5", value);
          filter += ")";
        }
        if(loanaccnum_op.selectedIndex > 0)
        {
          if(filter)
          {
            filter += " and ";
          }
          op = loanaccnum_op.selectedValue;
          field = "mps_loanaccountnumber";
          value = loanaccnum_val.value;

          filter += "(";
          filter += brokerengine.getFilterString(op, field+"1", value);
          filter += " or " + brokerengine.getFilterString(op, field+"2", value);
          filter += " or " + brokerengine.getFilterString(op, field+"3", value);
          filter += " or " + brokerengine.getFilterString(op, field+"4", value);
          filter += " or " + brokerengine.getFilterString(op, field+"5", value);
          filter += ")";
        }

        if(filter)
        {
          filter += " and (mps_ApplicationId ne null)";
          filter += " and (mps_defaultfundingposition eq 922600000)";
        }
        return filter;
      }

      function fetchFilteredApps(filter){
        var url = brokerengine.odataurl;
        var app_array = [];

        url +=  "/mps_fundingpositions?$select=_mps_applicationid_value&$filter="+filter;
        //console.log(url);
        $.ajax({
          async: false,
          dataType: "json",
          url: url,
          success: function(data){
            data.value.forEach(function(fp){
              app_array.push(fp._mps_applicationid_value)
            });
          }
        });

        //console.log(app_array);
        return app_array;
      }

    })(staticviews.app_fundingpos = staticviews.app_fundingpos || {} );

    (function(app_tasks){
      app_tasks.initialize = function(id)
      {
        brokerengine.staticviews.startRowLoading();

        brokerengine.staticviews.refresh_view();

        staticviews.grid = brokerengine.entities.applications.generateGrid(id, null, true, activeFilter);
        staticviews.grid.columns.moveElement(2, 4);

        var filter = new wijmo.grid.filter.FlexGridFilter(staticviews.grid);
        var cf = filter.getColumnFilter("Status").valueFilter;
        cf.showValues = {
          'Active': true,
        };
        filter.apply();
        brokerengine.staticviews.filter = filter;
        staticviews.grid.rowHeaders.columns.push(new wijmo.grid.Column(staticviews.rowSelectionColumn));
        staticviews.grid.selectionChanged.addHandler(staticviews.handleSelectionChange);

        var detailProvider = new wijmo.grid.detail.FlexGridDetailProvider(staticviews.grid,{
          isAnimated: true,
          createDetailCell: function(row)
          {
            return configureDetailsGrid(row.dataItem);
          }
        });

        brokerengine.staticviews.endRowLoading();
      };

      function configureDetailsGrid(appRow)
      {
        staticviews.startRowLoading();
        var cell = document.createElement('div');

        var colsarr = [
          { binding: "subject", name: "Subject", header: "Subject", width: "12*", minWidth: 300 },
          { binding: "activitytypecode", name: "Activity Type", header: "Activity Type", width: "5*", minWidth: 125 },
          { binding: "description", name: "Description", header: "Description", width: "20*", minWidth: 500 },
          { binding: "scheduledend", name: "Due Date", header: "Due Date", dataType: wijmo.DataType.Date, format: "dd-MM-yyyy", width: "5*", minWidth: 125 },
          { binding: "statecode", name: "Status", header: "Status", width: "5*", minWidth: 125 },
        ];

        var url = brokerengine.odataurl;
        var taskarr;

        var filter = "_regardingobjectid_value%20eq%20"+appRow.mps_applicationid;

        url +=  "/activitypointers?$filter="+filter;
        $.ajax({
          async: false,
          dataType: "json",
          url: url,
          success: function(data){
            taskarr = data.value;
            taskarr.forEach(function(task){
              if(task.scheduledend)
              {
                task.scheduledend = new Date(task.scheduledend);
              }
            });
          }
        });
        var taskview = new wijmo.collections.CollectionView(taskarr);
        var griddetails = new wijmo.grid.FlexGrid(cell, {
          columns: colsarr,
          autoGenerateColumns: false,
          itemsSource: taskarr,
          selectionMode: wijmo.grid.SelectionMode.ListBox,
          loadingRows: staticviews.startRowLoading,
          loadedRows: staticviews.endRowLoading,
          showAlternatingRows: true
        });

        var col = griddetails.columns.getColumn("Status");
        col.dataMap = new wijmo.grid.DataMap(brokerengine.statecode, "Code", "Status");

        staticviews.endRowLoading();
        return cell;
      }

      function activeFilter(app){
//        var display = staticviews.filter._filter(app) && app.statecode==0;
        var display = staticviews.filter._filter(app);
        return display;
      }

    })(staticviews.app_tasks = staticviews.app_tasks || {} );
  })(brokerengine.staticviews = brokerengine.staticviews || {});
})(window.brokerengine = window.brokerengine || {}, jQuery);
