(function(brokerengine, $, undefined){
  (function(staticviews)	{
    (function(leads){
      var loanamount_op = {};
      var loanamount_val = {};

      leads.initializeView = function() {
        brokerengine.staticviews.startRowLoading();

        initializeGrid();

        staticviews.handleExportButtons();
        staticviews.handleDeactivateButton(deactivateRecords);

        brokerengine.staticviews.endRowLoading();
      };

      function initializeGrid()
      {
        staticviews.title = "Leads";
        $("#be_staticviews_title").html("<H1>Leads</H1>");
        // initialize view filter controls
        //configureSubgridFilters();

        brokerengine.staticviews.refresh_view();

        // Should call brokerengine.entities.leads.generateGrid wit odata=true
        staticviews.grid = brokerengine.entities.leads.generateGrid("#be_view", null, true, subgridFilter);
        staticviews.grid.columns.moveElement(2, 4);

        staticviews.filter = new wijmo.grid.filter.FlexGridFilter(staticviews.grid);
        var cf = staticviews.filter.getColumnFilter("Status").valueFilter;
        cf.showValues = {
          'New': true,
          'Contacted': true,
          'Qualified': true,
          'Lost': true,
          'Cannot Contact': true,
          'No Longer Interested': true
        };
        staticviews.filter.apply();
        staticviews.grid.rowHeaders.columns.push(new wijmo.grid.Column(staticviews.rowSelectionColumn));
        staticviews.grid.selectionChanged.addHandler(staticviews.handleSelectionChange);

      }

      function subgridFilter(lead){
        var display = true;
        if(!$.isEmptyObject(staticviews.filter))
        {
          display = staticviews.filter._filter(lead);
        }
        if(staticviews.showFilteredResults)
        {
          return (staticviews.filterids.indexOf(lead.leadid) > -1) && display;
        }
        return display;
      }

      function configureSubgridFilters(){
        // initialize buttons. Provide callback function for Apply filters button
        staticviews.handleSubgridButtons(applySubgridFilters);

        if($.isEmptyObject(loanamount_op))
        {
          // Loan Amount
          //loanamount_op = brokerengine.createFilterOperator("#be_appview_op_loanamount", wijmo.DataType.Number);
          //loanamount_val = brokerengine.createFilterInput("#be_appview_val_loanamount", wijmo.DataType.Number, "c0");
        }
      }

      function applySubgridFilters()
      {
        var filter = getFilterString();
        if(filter)
        {
          staticviews.showFilteredResults = true;
          staticviews.filterids = fetchFilteredItems(filter);
          staticviews.selectedItems = [];
          staticviews.grid.itemsSource.refresh();
        }
      }

      function getFilterString(){
        var filter = ""
        var op;
        var value;
        return filter;
      }

      function fetchFilteredItems(filter){
        var url = brokerengine.odataurl;
        var items = [];

        url +=  "/leads?$select=leadid&$filter="+filter;
        //console.log(url);
        $.ajax({
          async: false,
          dataType: "json",
          url: url,
          success: function(data){
            data.value.forEach(function(item){
              items.push(item.leadid)
            });
          }
        });

        //console.log(app_array);
        return items;
      }


      function deactivateRecord(ent, id) {
        var entity = {};
        entity.statuscode = 7;
        entity.statecode = 2;
        var req = new XMLHttpRequest();
        req.open("PATCH", brokerengine.odataurl+"/"+
        ent+"("+id+")", true);
        req.setRequestHeader("OData-MaxVersion", "4.0");
        req.setRequestHeader("OData-Version", "4.0");
        req.setRequestHeader("Accept", "application/json");
        req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
        req.onreadystatechange = function () {
          if (this.readyState === 4) {
            req.onreadystatechange = null;
            if (this.status === 204) {
            }
            else {
              Xrm.Utility.alertDialog(this.statusText);
            }
          }
        };
        req.send(JSON.stringify(entity));
      }

      deactivateRecords=function() {
        if (confirm("Are you sure that you want to delete the selected records?")) {
          var height=staticviews.grid.cells.rows.length;
          for (var i=0;i<height;i++){
            if(staticviews.grid.rows[i].isSelected){
              deactivateRecord("leads",staticviews.grid.rows[i].dataItem.leadid);
            }
          }
        }
        initializeGrid();
      }

    })(staticviews.leads = staticviews.leads || {});

  })(brokerengine.staticviews = brokerengine.staticviews || {});
})(window.brokerengine = window.brokerengine || {}, jQuery);
