var LookupEditor = (function () {
    /**
     * Initializes a new instance of a LookupEditor.
     */
	function LookupEditor(flex, binding, fieldname, lookupentity, odatainterface) {
		var _this = this;
		// save references
		this._grid = flex;
		this._col = flex.columns.getColumn(binding);
		this._fieldname = fieldname;
		this._lookupentity = lookupentity;
		this._odatainterface = odatainterface;
		this._ecv = wijmo.tryCast(this._grid.collectionView, 'IEditableCollectionView');

		// connect to grid events
		this._grid.cellEditEnding.addHandler(this._cellEditEnding, this);
		this._grid.cellEditEnded.addHandler(this._cellEditEnded, this);
	}
	/*
	Object.defineProperty(LookupEditor.prototype, "control", {
		// gets an instance of the control being hosted by this grid editor
		get: function () {
			return this._ctl;
		},
		enumerable: true,
		configurable: true
	});
	*/
	// If grid data source is editable, commit any existing changes to currently edited item before 
	// saving lookup value changes
	LookupEditor.prototype._cellEditEnding = function (grid, args) {
		// check that this is our column
		if(args.cancel){
			return;
		}
		if (grid.columns[args.col] != this._col) {
			return;
		}

		var newvalue = grid.activeEditor.value;
		var newlookupid = this._col.dataMap.getKeyValue(newvalue);
		if(this._col.isRequired && ($.isEmptyObject(newvalue) || $.isEmptyObject(newlookupid))){
			alert("The field cannot be empty");
			args.cancel = true;
			args.stayInEditMode = true;
			return;
		}

		// Commit any existing edits to the item before applying new changes
		if(this._ecv)
		{
			this._ecv.commitEdit();
		}

		var item = grid.rows[args.row].dataItem;
		var fieldname = this._fieldname;
		var lookupentity = this._lookupentity;

//		this._grid.setCellData(args.row, args.col, newvalue);
		this._odatainterface.updateLookup(item, fieldname, lookupentity, newlookupid, this)
			.done(function(resp){
			})
			.fail(function(resp){
				alert("Unable to save changes. Received following error from the server:\r\n" + resp.responseJSON.Message);
			})
			.always(function(resp){
				this._grid.collectionView.refresh();
				this._grid.refresh(true);
			});
	};

	// Update grid cell data and save any changes to Odata source if available
	LookupEditor.prototype._cellEditEnded = function (grid, args) {
		// check that this is our column
		if (grid.columns[args.col] != this._col) {
			return;
		}
	};
	return LookupEditor;
}());