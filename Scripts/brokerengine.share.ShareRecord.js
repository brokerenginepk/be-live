(function(brokerengine, $, undefined){
	(function(share){
		/*
		Classes
		share.ShareRecord
			- entity
			- id
			- team
			- executes the actual share operation with the SOAP web service
		*/
		function ShareRecord(team)
		{

			ShareRecord.prototype.addShare = function(entity, recordid, callbacks){
				//alert(entity + ": " + recordid);

				var options = {
					accessRights: "ReadAccess WriteAccess AppendAccess AppendToAccess AssignAccess",
					principalEntityId: _this.team.teamid,
					principalEntityName: "team",
					targetEntityId: recordid,
					targetEntityName: entity
				};

				XrmServiceToolkit.Soap.GrantAccess(options);
			}

			this.team = team;
			var _this = this;
		}

		brokerengine.share.ShareRecord = ShareRecord;

		function ShareOperations()
		{
			ShareOperations.RetrieveSharedPrincipalsAndAccessRequestXml = function(_target){
				return ["<d:request xmlns:a=\"http://schemas.microsoft.com/xrm/2011/Contracts\" xmlns:b=\"http://schemas.datacontract.org/2004/07/System.Collections.Generic\" xmlns:d=\"http://schemas.microsoft.com/xrm/2011/Contracts/Services\">",
				"<a:Parameters>",
				  "<a:KeyValuePairOfstringanyType>",
					"<b:key>Target</b:key>",
					"<b:value i:type=\"a:EntityReference\">",
						"<a:Id>" + _target.Id + "</a:Id>",
						"<a:LogicalName>" + _target.LogicalName + "</a:LogicalName>",
						'<a:Name i:nil="true" />',
					"</b:value>",
				  "</a:KeyValuePairOfstringanyType>",
				"</a:Parameters>",
				"<a:RequestId i:nil=\"true\" />",
				"<a:RequestName>RetrieveSharedPrincipalsAndAccess</a:RequestName>",
			  "</d:request>"].join("");
			}

			ShareOperations.prototype.fetchShares = function(entity, recordid, onSuccess){
				this.targetId = recordid;
				this.targetEntity = entity;
				var target = { LogicalName: entity, Id: recordid};

				var reqXml = ShareOperations.RetrieveSharedPrincipalsAndAccessRequestXml(target);

				XrmServiceToolkit.Soap.Execute(reqXml, function(resp){
					var teams = [];

					var shares = resp.getElementsByTagName("c:Principal");
					for(var i=0; i<shares.length; i++){
						var team = {};
						team.teamid = shares[i].getElementsByTagName("a:Id")[0].textContent;
						team.name = shares[i].getElementsByTagName("a:Name")[0].textContent;
						teams.push(team);
					}
					onSuccess(teams);
				});

			}
		
			ShareOperations.prototype.processShare = function(entity, recordid, enableshare, onSuccess){
				if(enableshare){
					this.addShare(entity, recordid, onSuccess);
				}
				else{
					this.removeShare(entity, recordid, onSuccess);
				}
			}
			ShareOperations.prototype.addShare = function(entity, recordid, onSuccess){
				var additionalRights = "";
				if(entity=="mps_primarysecurity" || entity=="mps_fundingpositionnew"){
					additionalRights = " DeleteAccess";
				}
				var options = {
					accessRights: "ReadAccess WriteAccess AppendAccess AppendToAccess AssignAccess" + additionalRights,
					principalEntityId: _this.team.teamid,
					principalEntityName: "team",
					targetEntityId: recordid,
					targetEntityName: entity
				};

				XrmServiceToolkit.Soap.GrantAccess(options, onSuccess);
			}

			ShareOperations.prototype.removeShare = function(entity, recordid, onSuccess){
				var options = {
					revokeeEntityId: _this.team.teamid,
					revokeeEntityName: "team",
					targetEntityId: recordid,
					targetEntityName: entity
				};

				XrmServiceToolkit.Soap.RevokeAccess(options, onSuccess);
			}

			ShareOperations.prototype.setTeam = function(team){
				_this.team = team;
			}

			var _this = this;
			this.team = {};
			this.targetEntity = "";
			this.targetId = "";
		}

		brokerengine.share.ShareOperations = ShareOperations;

	})(brokerengine.share = brokerengine.share || {});

})(window.brokerengine = window.brokerengine || {}, jQuery);
