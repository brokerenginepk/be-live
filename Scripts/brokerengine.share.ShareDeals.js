(function(brokerengine, $, undefined){
	(function(share){
		/*
		Classes
		share.ShareRecord
			- entity
			- id
			- team
			- executes the actual share operation with the SOAP web service

		share.ShareDeals
		share.ShareLeads
			- handle multiple items
			- call ShareRecord for actual sharing
			- iterate for all related entities
			- fetchShares is implemented here

		*/
		function ShareApps()
		{
			ShareApps.prototype.initialize = function(isdialog){
				// Disable the Share button. It is only made available once a deal is available for processing
				this.shareui.enableShare(false);

				// Initialize the dialog / form.
				// 		- But do not open the dialog.
				// 		- Since no deal is available at this time, shares would not be loaded yet 
				if(isdialog){
					this.shareui.initializeDialog();
				}
				else{
					this.shareui.initializeForm();
				}

				// Now fetch deal if this was opened as a form
				this.params = brokerengine.getQueryParams();
				if(this.params.id){
					this.fetchDeal();
				}
			}

			ShareApps.prototype.openDialog = function(deals){
				this.setDeals(deals);
				this.shareui.openDialog();
			}

			ShareApps.prototype.onAddShare = function(ev){
				var team = _this.shareui.teamSelect.selectedItem;
				_this.shareOps.setTeam(team);

				_this.deals.forEach(function(deal){
					_this.shareDeal(deal, true);
					_this.notifyProcessor(deal, team);
				});
			}

			ShareApps.prototype.onRemoveShare = function(ev){
				var shares = _this.shareui.getSelectedShares();
				shares.forEach(function(share){
					_this.shareOps.setTeam(share);

					var dealid = share.itemId;
					var deal = _this.findDeal(dealid);

					_this.shareDeal(deal, false);
				});
			}

			ShareApps.prototype.shareDeal = function(deal, enableshare){
				// enableshare = true => grant access
				// enableshare = false => revoke access
				var lead = deal._mps_lead_value;
				var history = deal._bbb_stagedurationhistory_value;
				var parent = deal._mps_contact_value;
				var agent = deal._mps_contactforaccess_value;
				var solicitor = deal._mps_solicitordetails_value;
				var lenderdetails = deal._mps_brokerlenderdetails_value;

				this.onShareOpStart();
				this.shareOps.processShare("mps_application", deal.mps_applicationid, enableshare, function(result){
					_this.onShareOpComplete(result);
				});
				this.shareChildRecords(deal, enableshare);
				if(lead){
					this.onShareOpStart();
					this.shareOps.processShare("lead", lead, enableshare, function(result){
						_this.onShareOpComplete(result);
					});
				}
				if(history){
					this.onShareOpStart();
					this.shareOps.processShare("bbb_applicationstageduration", history, enableshare, function(result){
						_this.onShareOpComplete(result);
					});
				}
				if(parent){
					this.onShareOpStart();
					this.shareOps.processShare("contact", parent, enableshare, function(result){
						_this.onShareOpComplete(result);
					});
					brokerengine.entities.contacts.fetchById(parent, function(item){
						if(item._mps_brand_value){
							this.onShareOpStart();
							this.shareOps.processShare("mps_brand", item._mps_brand_value, enableshare, function(result){
								_this.onShareOpComplete(result);
							});
						}
						if(item._mps_referredby_value){
							this.onShareOpStart();
							this.shareOps.processShare("contact", item._mps_referredby_value, enableshare, function(result){
								_this.onShareOpComplete(result);
							});
						}
					}, "_mps_brand_value,_mps_referredby_value");
				}
				if(agent){
					this.onShareOpStart();
					this.shareOps.processShare("contact", agent, enableshare, function(result){
						_this.onShareOpComplete(result);
					});
				}
				if(solicitor){
					this.onShareOpStart();
					this.shareOps.processShare("contact", solicitor, enableshare, function(result){
						_this.onShareOpComplete(result);
					});
				}
				if(lenderdetails){
					this.onShareOpStart();
					this.shareOps.processShare("mps_brokerlenderdetails", lenderdetails, enableshare, function(result){
						_this.onShareOpComplete(result);
					});
				}
			}

			ShareApps.prototype.notifyProcessor = function(deal, team){
				var newrec = {
					"bbb_name": deal.mps_name + " shared with " + team.name,
					"bbb_SharedDeal@odata.bind": "/mps_applications(" + deal.mps_applicationid + ")",
					"bbb_ProcessorTeam@odata.bind": "/teams(" + team.teamid + ")"
				}

				this.odataShareNotification.createRecord(newrec, this);
			}

			ShareApps.prototype.shareChildRecords = function(deal, enableshare){
				// This function is needed because Cascading of Share is not working
				//  brokerengine.getData(url, callback);

				var dealurl = brokerengine.odataurl + "/mps_applications(" + deal.mps_applicationid + ")";
				var url = "";

				// Handle security properties
				url = dealurl + "/mps_mps_application_mps_securityproperty";
				brokerengine.getData(url, function(items){
					items.forEach(function(item){
						_this.shareOps.processShare("mps_securityproperty", item.mps_securitypropertyid, enableshare);
					});
				});

				url = dealurl + "/mps_mps_application_mps_primarysecurity_Application";
				brokerengine.getData(url, function(items){
					items.forEach(function(item){
						_this.shareOps.processShare("mps_primarysecurity", item.mps_primarysecurityid, enableshare);
					});
				});

				// Handle funding positions
				url = dealurl + "/mps_mps_application_mps_fundingposition";
				brokerengine.getData(url, function(items){
					items.forEach(function(item){
						_this.shareOps.processShare("mps_fundingposition", item.mps_fundingpositionid, enableshare);
					});

				});

				url = dealurl + "/mps_mps_application_mps_fundingpositionnew_Application";
				brokerengine.getData(url, function(items){
					items.forEach(function(item){
						_this.shareOps.processShare("mps_fundingpositionnew", item.mps_fundingpositionnewid, enableshare);
					});

				});

				// Handle constructions
				url = dealurl + "/mps_mps_application_mps_construction";
				brokerengine.getData(url, function(items){
					items.forEach(function(item){
						_this.shareOps.processShare("mps_construction", item.mps_constructionid, enableshare);
					});

				});

				// Handle variations
				url = dealurl + "/mps_mps_application_mps_variations";
				brokerengine.getData(url, function(items){
					items.forEach(function(item){
						_this.shareOps.processShare("mps_variations", item.mps_variationsid, enableshare);
					});

				});
				
				// Handle financial profile
				url = dealurl + "/mps_mps_application_mps_financialprofile";
				brokerengine.getData(url, function(items){
					items.forEach(function(item){
						_this.shareOps.processShare("mps_financialprofile", item.mps_financialprofileid, enableshare);
					});

				});

				// Handle bank account
				url = dealurl + "/mps_mps_application_mps_bankaccount";
				brokerengine.getData(url, function(items){
					items.forEach(function(item){
						_this.shareOps.processShare("mps_bankaccount", item.mps_bankaccountid, enableshare);
					});

				});

				// Handle client objectives
				url = dealurl + "/mps_mps_application_mps_clientobjective";
				brokerengine.getData(url, function(items){
					items.forEach(function(item){
						_this.shareOps.processShare("mps_clientobjective", item.mps_clientobjectiveid, enableshare);
					});

				});

				// Handle checklist
				url = dealurl + "/mps_mps_application_new_checklist_ApplicationId";
				brokerengine.getData(url, function(items){
					items.forEach(function(item){
						_this.shareOps.processShare("new_checklist", item.new_checklistid, enableshare);
					});

				});

				url = dealurl + "/mps_mps_application_mps_notes";
				brokerengine.getData(url, function(items){
					items.forEach(function(item){
						_this.shareOps.processShare("mps_notes", item.mps_notesid, enableshare);
					});

				});

				// Handle activities
				url = dealurl + "/mps_application_ActivityPointers";
				brokerengine.getData(url, function(items){
					items.forEach(function(item){
						var entity = item["@odata.type"].replace("#Microsoft.Dynamics.CRM.", "");
						_this.shareOps.processShare(entity, item.activityid, enableshare);
					});

				});
			}

			ShareApps.prototype.onShareOpStart = function(){
				this.shareOpsCounter++;
				$("#waitLogo").show();
			}

			ShareApps.prototype.onShareOpComplete = function(result){
				if(this.shareOpsCounter > 0){
					this.shareOpsCounter--;
				}
				if(this.shareOpsCounter == 0)
				{
					$("#waitLogo").hide();
					_this.fetchShares();
				}
			}

			ShareApps.prototype.setDeals = function(deals){
				this.deals = deals;
				this.fetchShares();
				this.shareui.enableShare(true);
			}

			ShareApps.prototype.fetchDeal = function(){
				var deal = {};

				brokerengine.entities.applications.fetchById(this.params.id, function(deal){
					_this.setDeals([deal]);
				});
			}

			ShareApps.prototype.fetchShares = function(){
				_this.shareui.emptyShare();
				this.deals.forEach(function(deal){
					$("#waitLogo").show();
					_this.shareOps.fetchShares("mps_application", deal.mps_applicationid, function(shares){
						var shareHeader = {};
						shareHeader.name = deal.mps_name;
						shareHeader.header = true;
						_this.shareui.addShare(shareHeader);
						shares.forEach(function(share){
							share.itemName = deal.mps_name;
							share.itemId = deal.mps_applicationid;
							_this.shareui.addShare(share);
						});
						$("#waitLogo").hide();
					});
				});
			}

			ShareApps.prototype.findDeal = function(dealid){
				for(var i=0; i<this.deals.length; i++){
					if(this.deals[i].mps_applicationid == dealid){
						return this.deals[i];
					}
				}
			}

			var _this = this;
			this.deals = [];
//			this.processDeal = false;
			this.params = {};
//			this.shareRecord = {};
			this.shareOps = new brokerengine.share.ShareOperations();
			this.shareOpsCounter = 0;
			this.odataShareNotification = new brokerengine.ODataInterface("bbb_sharenotifications");
			this.shareui = new brokerengine.share.ShareUI({
				elemSelect: "#be-share-selectteam",
				elemShared: "#be-share-sharedteams",
				btnAdd: "#btnAddSharingTeam",
				btnRemove: "#btnRemoveSharingTeam",
				btnDone: "#btnCloseShareDialog",
				onAddShare: this.onAddShare,
				onRemoveShare: this.onRemoveShare
			});
		}

		brokerengine.share.ShareApps = ShareApps;
	})(brokerengine.share = brokerengine.share || {});

})(window.brokerengine = window.brokerengine || {}, jQuery);
