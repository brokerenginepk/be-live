(function(brokerengine, $, undefined)
{
  //brokerengine.baseurl = "https://modus360-dev.xreonline.com.au";
  brokerengine.baseurl = "https://brokerengine.xreonline.com.au";

  brokerengine.odataurl = brokerengine.baseurl + "/api/data/v8.2";
  brokerengine.pagesize = 50;

  brokerengine.purpose = [
    { Label: "--Select--", Value: "" },
    { Label: "Owner Occupier", Value: 922600000 },
    { Label: "Investment", Value: 922600001 }
  ];
  brokerengine.purposemap = new wijmo.grid.DataMap(brokerengine.purpose, "Value", "Label");

  brokerengine.fundingtemplate = [
    { Label: "1. Purchase", Value: 922600000 },
    { Label: "2. Refinance and Debt Consolidation", Value: 922600001 },
    { Label: "3. Construction (own land)", Value: 922600002 },
    { Label: "4. Land and Build", Value: 922600003 },
    { Label: "5. Top Up", Value: 922600004 }
  ];

  brokerengine.pmtoptions = [
    { Label:"Principal and Interest", Value: 922600000 },
    { Label:"1Year Interest Only", Value: 922600001 },
    { Label:"2Years Interest Only", Value: 922600002 },
    { Label:"3Years Interest Only", Value: 922600003 },
    { Label:"4Years Interest Only", Value: 922600004 },
    { Label:"5Years Interest Only", Value: 922600005 },
    { Label:"10Years Interest Only", Value: 922600006 },
    { Label:"15Years Interest Only", Value: 922600007 },
    { Label:"Interest Only (LOC)", Value: 922600008 }
  ];

  brokerengine.producttype = [
    { Label:"--Select--", Value: "" },
    { Label:"Standard Variable", Value: 922600000 },
    { Label:"Basic Variable", Value: 922600001 },
    { Label:"Line of Credit", Value: 922600002 },
    { Label:"Introductory Variable", Value: 922600003 },
    { Label:"1Year Fixed Rate", Value: 922600004 },
    { Label:"2Year Fixed Rate", Value: 922600005 },
    { Label:"3Year Fixed Rate", Value: 922600006 },
    { Label:"4Year Fixed Rate", Value: 922600007 },
    { Label:"5Year Fixed Rate", Value: 922600008 },
    { Label:"10Year Fixed Rate", Value: 922600009 },
    { Label:"15Year Fixed Rate", Value: 922600010 }
  ];

  brokerengine.reviewfrequency = [
    { Label:"3 Monthly", Value: 1},
    { Label:"6 Monthly", Value: 2},
    { Label:"12 Monthly", Value: 3},
    { Label:"Expiry of Fixed Rates/IO Period Only", Value: 4},
    { Label:"No Review", Value: 5}
  ];

  brokerengine.status = [
    { Value: 0, Label: "Active" },
    { Value: 1, Label: "Inactive" },
  ];
  brokerengine.statusmap = new wijmo.grid.DataMap(brokerengine.status, "Value", "Label");

  brokerengine.statecode = [
    { Code: 0, Status: "Open" },
    { Code: 1, Status: "Completed" },
    { Code: 2, Status: "Canceled" },
    { Code: 3, Status: "Scheduled" }
  ];

  brokerengine.createFilterOperator = function(element, datatype)
  {
    var list = wijmo.culture.FlexGridFilter.stringOperators;
    if (datatype == wijmo.DataType.Date) {
      list = wijmo.culture.FlexGridFilter.dateOperators;
    }
    else if (datatype == wijmo.DataType.Number) {
      list = wijmo.culture.FlexGridFilter.numberOperators;
    }
    else if (datatype == wijmo.DataType.Boolean) {
      list = wijmo.culture.FlexGridFilter.booleanOperators;
    }
    // create and initialize the combo
    var cmb = new wijmo.input.ComboBox(element);
    cmb.itemsSource = list;
    cmb.displayMemberPath = 'name';
    cmb.selectedValuePath = 'op';
    // return combo
    return cmb;
  }

  brokerengine.createFilterInput = function (element, datatype, format)
  {
    var ctl = null;
    if (datatype == wijmo.DataType.Date)
    {
      ctl = new wijmo.input.InputDate(element);
      ctl.format = format;
    }
    else if (datatype == wijmo.DataType.Number)
    {
      ctl = new wijmo.input.InputNumber(element);
      ctl.format = format;
    }
    else
    {
      ctl = new wijmo.input.ComboBox(element);
    }
    ctl.isRequired = false;
    return ctl;
  }

  brokerengine.createDatamapInput = function(element, obj_array, value_field, label_field)
  {
    var ctl = new wijmo.input.ComboBox(element);
    ctl.itemsSource = obj_array;
    ctl.displayMemberPath = label_field;
    ctl.selectedValuePath = value_field;
    ctl.isRequired = false;
    return ctl;

  }

  brokerengine.formatComboBox = function(s, e){
    var item = e.data;
    if(item.header){
      e.item.classList.add("be_combo_header");
      e.item.classList.add("wj-state-disabled");
      e.item.disabled = true;
    }
  }

  brokerengine.formatListBox = function(s, e){
    var item = e.data;
    if(item.header){
      e.item.innerHTML  = "<label>" + item.name + "</label>";
      e.item.classList.add("be_list_header");
      e.item.classList.add("wj-state-disabled");
      e.item.disabled = true;
    }
  }

  brokerengine.getFilterString = function(op, fld, val)
  {
    switch (op) {
      case 0:
      return fld + ' eq ' + val;
      case 1:
      return fld + ' ne ' + val;
      case 2:
      return fld + ' gt ' + val;
      case 3:
      return fld + ' ge ' + val;
      case 4:
      return fld + ' lt ' + val;
      case 5:
      return fld + ' le ' + val;
      case 6:
      return 'startswith(' + fld + ',' + val + ')';
      case 7:
      return 'endswith(' + fld + ',' + val + ')';
      case 8:
      return 'contains(' + fld + ',' + val + ')';
      case 9:
      return 'not contains(' + fld + ',' + val + ')';
    }
  }

  // val = JS date object
  brokerengine.getFilterDate = function(op, fld, val)
  {
    var datestr = val.format("yyyy-MM-dd"),
    start = datestr + "T00:00:00Z",
    end = datestr + "T23:59:59Z";
    var filter;
    switch (op) {
      case 0: // wijmo.grid.filter.Operator.EQ:
      filter = "(" + fld + " ge " + start + ")";
      filter += "and";
      filter += "(" + fld + " le " + end + ")";
      break;
      case 2: // wijmo.grid.filter.Operator.GT:
      filter = fld + " gt " + end;
      break;
      case 4: //wijmo.grid.filter.Operator.LT:
      filter = fld + " lt " + start;
      break;
    }
    return filter;
  }

  brokerengine.getQueryParams = function(){
    var querystr = location.search.substr(1).split("&");
    /*
    for (var i in params) {
      params[i] = params[i].replace(/\+/g, " ").split("=");
     }
     */

    var params = {};
    querystr.forEach(function(param){
      param = param.replace(/\+/g, " ").split("=");
      params[param[0]] = param[1];
    });

    return params;
  }

  brokerengine.getData = function(url, onsuccess)
  {
    $.ajax({
      url: url,
      dataType: "json"
    })
      .done(function(response){
        var data = response.value;
        if(response["@odata.nextLink"])
        {
          brokerengine.getData(response["@odata.nextLink"], function(datanextpage){
            data = data.concat(datanextpage);
            onsuccess(data);            
          });
        }
        else
        {
          onsuccess(data);
        }
      });
}
})(window.brokerengine = window.brokerengine || {}, jQuery);
